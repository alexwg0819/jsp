<%-- 
    Document   : showReport
    Created on : 2019/12/13, 上午 08:47:56
    Author     : 85253
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="ict.bean.*, java.util.*, java.text.*"%>
<%@ taglib uri="/WEB-INF/tlds/teacherBar" prefix="ict" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lesson</title>
        <script>
            function showAttendent(LessionID){
                document.getElementById("form" + LessionID).submit();
            }
        </script>
    </head>
    <body>
        <script>
            <%
                if ((String) request.getAttribute("message") != null) {
            %>
            alert("<%=(String) request.getAttribute("message")%>");
            <%
                }
            %>
        </script>
        <jsp:useBean scope="session" id="userInfo" class="ict.bean.TeacherInfo"/>
        <ict:teacherBar name="<%=userInfo.getName()%>"/>
        <%
           
            ArrayList<Attendent> lesson = (ArrayList) request.getAttribute("lesson");
            ArrayList<Report> student =  (ArrayList) request.getAttribute("Student");
            ArrayList<Attendent> attendentl ;
//             String module =  request.getParameter("");
        %>
        <center>
<button style="width:100%;background-color: blueviolet"><h2 style="color:white">Report for attendance rate less than 60% </h2></button>
            <table style="width:100%  " border="1">
                <tr>
                    <th style="background-color: buttonshadow">ID</th>
                    <th style="background-color: buttonshadow">Name</th>
                    <th style="background-color: buttonshadow">Attendent</th>
                    
                </tr>
                    <%
                        double totalDuration = 0;

                        for (int i = 0; i < lesson.size(); i++) {
//                            if ((i+1) % 3 == 0) {
//                                out.print("</tr><tr>");
                            totalDuration += lesson.get(i).getLessonDuration();
                        }
                         for (int i = 0; i < student.size(); i++) {
                             %>
                             <%if(student.get(i).showAttd(totalDuration) <= 60){ %>
                            <tr>
                            <td align="center">
                            <% out.print(student.get(i).getID());%>
                            </td>
                            <td align="center">
                            <% out.print(student.get(i).getStudentName()); %>
                            </td>
                            <td align="center">
                             <% out.print(student.get(i).showAttd(totalDuration)+"%");   %>
                            </td>
                            </tr>
                             <%} %>
                            <%
                        }
                    %>
                    
                    ProgramID : <%=request.getParameter("programID") %> <br>
                    ModuleID : <%=request.getParameter("moduleID") %> <br>
                    Class : <%=request.getParameter("classID") %><br>
                    Total Duration of This Program ( <%= totalDuration %> hours )
            </table>
                     <form action="teacher" method="get" id="showFail">
                        <input type="hidden" name="action" value="showReport"/>
                        <input type="hidden" name="moduleID" value="<%=request.getParameter("moduleID") %>"/>
                        <input type="hidden" name="programID" value="<%=request.getParameter("programID") %> "/>
                        <input type="hidden" name="classID" value="<%=request.getParameter("classID") %>"/>
                    </form>
<button style="width:100%;background-color: blue" onclick="document.getElementById('showFail').submit();" ><h2 style="color:white">--->Click Here to Show Report for all Student <---</h2></button>
        </center>
    </body>
</html>
