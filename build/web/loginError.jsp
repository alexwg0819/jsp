<%-- 
    Document   : loginError
    Created on : 2019/11/29, 上午 11:15:40
    Author     : tommylam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Error</title>
    </head>
    <body>
        <p>Incorrect Password</p>
        <p>
            <a href="login.jsp">Login again</a>
        </p>
    </body>
</html>
