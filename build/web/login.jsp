<%-- 
    Document   : loginPage
    Created on : 2019/12/3, 下午 05:21:15
    Author     : tommylam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Page</title>
    </head>
    <body>
    <center><h1>Welcome!</h1></center>
    <center>
        <form method="post" action="main">
            <input type="hidden" name="action" value="authenticate"/>
            <table>
                <tr>
                    <td>ID : </td>
                    <td><input type="text" name="id" style="width: 100%"/></td>
                </tr>
                <tr>
                    <td>Password : </td>
                    <td><input type="password" name="password" style="width: 100%"/></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <label>Student<input type="radio" name="role" value="Student" checked/></label>
                        <label>Teacher<input type="radio" name="role" value="Teacher"/></label>
                        <label>Administrator<input type="radio" name="role" value="Admin"/></label>
                    </td>
                </tr>
                <tr>
                    <td><input type="reset" value="Reset" style="width: 100%; background-color: lightcoral"/></td>
                    <td><input type="submit" value="Login" style="width: 100%; background-color: lightgreen"/></td>
                </tr>
            </table>
        </form>
    </center>
</body>
</html>
