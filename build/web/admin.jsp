<%-- 
    Document   : student
    Created on : 2019/12/4, 下午 06:16:45
    Author     : tommylam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/adminBar" prefix="ict" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Admin</title>
    </head>
    <body>
        <jsp:useBean scope="session" id="userInfo" class="ict.bean.AdminInfo"/>
        <ict:adminBar name="<%=userInfo.getName()%>"/>
                <script>
            <%
                String message = (String) request.getAttribute("check");
                if (message != null) {
            %>
            alert("<%=message%>");
            <%
                }
            %>
        </script>
        <h1>

        </h1>
    </body>
</html>
