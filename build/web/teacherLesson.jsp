<%-- 
    Document   : teacherLesson
    Created on : 2019/12/6, 下午 10:40:06
    Author     : tommylam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="ict.bean.*, java.util.*, java.text.*"%>
<%@ taglib uri="/WEB-INF/tlds/teacherBar" prefix="ict" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lesson</title>
        <script>
            function showAttendent(LessionID){
                document.getElementById("form" + LessionID).submit();
            }
        </script>
    </head>
    <body>
        <script>
            <%
                if ((String) request.getAttribute("message") != null) {
            %>
            alert("<%=(String) request.getAttribute("message")%>");
            <%
                }
            %>
        </script>
        <jsp:useBean scope="session" id="userInfo" class="ict.bean.TeacherInfo"/>
        <ict:teacherBar name="<%=userInfo.getName()%>"/>
        <%
            ArrayList<Attendent> lesson = (ArrayList) request.getAttribute("lesson");
        %>
        <center>
            <form action="teacher" method="get">
                <input type="hidden" name="action" value="lesson"/>
                <input type="hidden" name="search" value="date"/>
                <input type="hidden" name="moduleID" value="<%=request.getParameter("moduleID")%>"/>
                <input type="hidden" name="programID" value="<%=request.getParameter("programID")%>"/>
                <input type="hidden" name="classID" value="<%=request.getParameter("classID")%>"/>
                <label>From : <input type="date" name="fDate" required/></label>
                <label>To : <input type="date" name="tDate" required/></label>
                <input type="submit" value="Serach"/>
            </form>
            <form action="teacher" method="get">
                <input type="hidden" name="action" value="lesson"/>
                <input type="hidden" name="moduleID" value="<%=request.getParameter("moduleID")%>"/>
                <input type="hidden" name="programID" value="<%=request.getParameter("programID")%>"/>
                <input type="hidden" name="classID" value="<%=request.getParameter("classID")%>"/>
                <input type="submit" value="Cancel Serach"/>
            </form>
            <table>
                <tr>
                    <td style="width: 600px; height: 200px; font-size: 20px; background-color: lightgreen;" align="center">
                        <form action="teacher" method="get">
                            <input type="hidden" name="action" value="addLesson"/>
                            <input type="hidden" name="moduleID" value="<%=request.getParameter("moduleID")%>"/>
                            <input type="hidden" name="programID" value="<%=request.getParameter("programID")%>"/>
                            <input type="hidden" name="classID" value="<%=request.getParameter("classID")%>"/>
                            <table>
                                <tr>
                                    <td>Date:</td>
                                    <td><input type="date" name="lessonDate" required/></td>
                                </tr>
                                <tr>
                                    <td>Time:</td>
                                    <td><input type="time" name="lessonTime" min="08:00" max="20:00" required/></td>
                                </tr>
                                <tr>
                                    <td>Duration:</td>
                                    <td><input type="number" name="lessonDuration" min="1" max="3" step="0.5" required/> Hour(s)</td>
                                </tr>
                            </table>
                            <hr/>
                            <input type="reset" value="Clear"/>
                            <input type="submit" value="New Lesson"/>
                        </form>
                    </td>
                    <%
                        if (lesson.size() == 0) {
                            out.print("<td>No Lesson Found</td>");
                        }
                        for (int i = 0; i < lesson.size(); i++) {
                            if ((i+1) % 3 == 0) {
                                out.print("</tr><tr>");
                            }
                    %>
                    <td>
                        <form action="teacher" method="get" id="form<%=lesson.get(i).getLessonID()%>">
                            <input type="hidden" name="action" value="attendent"/>
                            <input type="hidden" name="LessonID" value="<%=lesson.get(i).getLessonID()%>"/>
                        </form>
                        <button style="width: 600px; height: 200px; font-size: 20px" onclick="showAttendent('<%=lesson.get(i).getLessonID()%>')">
                            <center>
                                <table>
                                    <tr>
                                        <td>Date:</td>
                                        <td><%=new SimpleDateFormat("yyyy-MM-dd").format(lesson.get(i).getLessonDate())%></td>
                                    </tr>
                                    <tr>
                                        <td>Time:</td>
                                        <td><%=new SimpleDateFormat("HH:mm").format(lesson.get(i).getLessonTime())%></td>
                                    </tr>
                                    <tr>
                                        <td>Duration:</td>
                                        <td><%=lesson.get(i).getLessonDuration()%> Hour(s)</td>
                                    </tr>
                                </table>
                                <hr/>
                                <font style="font-weight: bold">Click to show Attendent</font>
                            </center>
                        </button>
                    </td>
                    <%}%>
                </tr>
            </table>
        </center>
    </body>
</html>
