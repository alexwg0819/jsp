<%-- 
    Document   : teacherAttendent
    Created on : 2019/12/6, 下午 09:34:26
    Author     : tommylam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="ict.bean.*, java.util.*, java.text.*"%>
<%@ taglib uri="/WEB-INF/tlds/teacherBar" prefix="ict" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Attendent</title>
        <script>
            function showLesson(programID, classID) {
                document.getElementById("form" + programID + classID).submit();
            }
        </script>
    </head>
    <body>
        <jsp:useBean scope="session" id="userInfo" class="ict.bean.TeacherInfo"/>
        <ict:teacherBar name="<%=userInfo.getName()%>"/>
        <%
            ArrayList<ClassInfo> classInfo = (ArrayList) request.getAttribute("classInfo");
        %>
    <center>
        <form action="teacher" method="get">
            Search : 
            <input type="hidden" name="action" value="class"/>
            <input type="hidden" name="search" value="search"/>
            <input type="hidden" name="moduleID" value="<%=request.getParameter("moduleID")%>"/>
            <input type="text" name="value" style="width: 80%" placeholder="Search : "/>
            <input type="submit" value="Search" style="width: 10%"/>
        </form>
        <form action="teacher" method="get" id="cancelSearch">
            <input type="hidden" name="action" value="class"/>
            <input type="hidden" name="moduleID" value="<%=request.getParameter("moduleID")%>"/>
            <input type="submit" value="Cancel Serach"/>
        </form>
        <table>
            <tr>
                <%
                    if (classInfo.size() == 0) {
                        out.print("<td>No Class Found</td>");
                    }
                    for (int i = 0; i < classInfo.size(); i++) {
                        if (i % 3 == 0) {
                            out.print("</tr><tr>");
                        }
                %>
                <td>
                    <form action="teacher" method="get" id="form<%=classInfo.get(i).getProgramID()%><%=classInfo.get(i).getClassID()%>">
                        <input type="hidden" name="action" value="lesson"/>
                        <input type="hidden" name="moduleID" value="<%=classInfo.get(i).getModuleID()%>"/>
                        <input type="hidden" name="programID" value="<%=classInfo.get(i).getProgramID()%>"/>
                        <input type="hidden" name="classID" value="<%=classInfo.get(i).getClassID()%>"/>
                    </form>
                    <button style="width: 600px; height: 200px; font-size: 20px" onclick="showLesson('<%=classInfo.get(i).getProgramID()%>', '<%=classInfo.get(i).getClassID()%>')">
                        <center>
                            <table>
                                <tr>
                                    <td>Program Name:</td>
                                    <td><%=classInfo.get(i).getProgramID()%></td>
                                </tr>
                                <tr>
                                    <td>Class:</td>
                                    <td><%=classInfo.get(i).getProgramName()%>-<%=classInfo.get(i).getClassID()%></td>
                                </tr>
                            </table>
                            <hr/>
                            <font style="font-weight: bold">Click to show Lesson</font>
                        </center>
                    </button>
                </td>
                <%}%>
            </tr>
        </table>
    </body>
</html>
