package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import ict.bean.*;
import java.util.*;
import java.text.*;

public final class studModule_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(1);
    _jspx_dependants.add("/WEB-INF/tlds/studBar.tld");
  }

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("        <title>Module</title>\r\n");
      out.write("        <script>\r\n");
      out.write("            function attentend(moduleID){\r\n");
      out.write("                document.getElementById(\"form\" + moduleID).submit();\r\n");
      out.write("            }\r\n");
      out.write("        </script>\r\n");
      out.write("    </head>\r\n");
      out.write("    <body>\r\n");
      out.write("        ");
      ict.bean.StudentInfo userInfo = null;
      synchronized (session) {
        userInfo = (ict.bean.StudentInfo) _jspx_page_context.getAttribute("userInfo", PageContext.SESSION_SCOPE);
        if (userInfo == null){
          userInfo = new ict.bean.StudentInfo();
          _jspx_page_context.setAttribute("userInfo", userInfo, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n");
      out.write("        ");
      //  ict:studBar
      ict.tag.studbar _jspx_th_ict_studBar_0 = (_jspx_resourceInjector != null) ? _jspx_resourceInjector.createTagHandlerInstance(ict.tag.studbar.class) : new ict.tag.studbar();
      _jspx_th_ict_studBar_0.setJspContext(_jspx_page_context);
      _jspx_th_ict_studBar_0.setName(userInfo.getName());
      _jspx_th_ict_studBar_0.doTag();
      out.write("\r\n");
      out.write("        <center>\r\n");
      out.write("            <form action=\"student\" method=\"get\">\r\n");
      out.write("                Search : \r\n");
      out.write("                <input type=\"hidden\" name=\"action\" value=\"module\"/>\r\n");
      out.write("                <input type=\"hidden\" name=\"search\" value=\"search\"/>\r\n");
      out.write("                <input type=\"text\" name=\"value\" style=\"width: 80%\" placeholder=\"Search : \"/>\r\n");
      out.write("                <input type=\"submit\" value=\"Search\" style=\"width: 10%\"/>\r\n");
      out.write("            </form>\r\n");
      out.write("            <form action=\"student\" method=\"get\" id=\"cancelSearch\">\r\n");
      out.write("                <input type=\"hidden\" name=\"action\" value=\"module\"/>\r\n");
      out.write("                <input type=\"submit\" value=\"Cancel Serach\"/>\r\n");
      out.write("            </form>\r\n");
      out.write("            <table>\r\n");
      out.write("                <tr>\r\n");
      out.write("                    ");

                        ArrayList<LessonInfo> module = (ArrayList) request.getAttribute("Module");
                        if (module.size() == 0) {
                            out.print("<td>No Module Found</td>");
                        }
                        for (int i = 0; i < module.size(); i++) {
                            if (i % 3 == 0) {
                                out.print("</tr><tr>");
                            }
                    
      out.write("\r\n");
      out.write("                    <td>\r\n");
      out.write("                        <form action=\"student\" method=\"get\" id=\"form");
      out.print(module.get(i).getModuleID());
      out.write("\">\r\n");
      out.write("                            <input type=\"hidden\" name=\"action\" value=\"attendent\"/>\r\n");
      out.write("                            <input type=\"hidden\" name=\"moduleID\" value=\"");
      out.print(module.get(i).getModuleID());
      out.write("\"/>\r\n");
      out.write("                        </form>\r\n");
      out.write("                        <button style=\"width: 600px; height: 200px; font-size: 20px\" onclick=\"attentend('");
      out.print(module.get(i).getModuleID());
      out.write("')\">\r\n");
      out.write("                            <center>\r\n");
      out.write("                                <table>\r\n");
      out.write("                                    <tr>\r\n");
      out.write("                                        <td>Module ID:</td>\r\n");
      out.write("                                        <td>");
      out.print(module.get(i).getModuleID());
      out.write("</td>\r\n");
      out.write("                                    </tr>\r\n");
      out.write("                                    <tr>\r\n");
      out.write("                                        <td>Module Name:</td>\r\n");
      out.write("                                        <td>");
      out.print(module.get(i).getModuleName());
      out.write("</td>\r\n");
      out.write("                                    </tr>\r\n");
      out.write("                                    <tr>\r\n");
      out.write("                                        <td>Teacher:</td>\r\n");
      out.write("                                        <td>");
      out.print(module.get(i).getTeacherName());
      out.write("</td>\r\n");
      out.write("                                    </tr>\r\n");
      out.write("                                </table>\r\n");
      out.write("                                <hr/>\r\n");
      out.write("                                <font style=\"font-weight: bold\">Click to check Attendent</font>\r\n");
      out.write("                            </center>\r\n");
      out.write("                        </button>\r\n");
      out.write("                    </td>\r\n");
      out.write("                    ");
}
      out.write("\r\n");
      out.write("                </tr>\r\n");
      out.write("            </table>\r\n");
      out.write("        </center>\r\n");
      out.write("    </body>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
