<%-- 
    Document   : student
    Created on : 2019/12/4, 下午 06:16:45
    Author     : tommylam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="ict.bean.*, java.util.*, java.text.*"%>
<%@ taglib uri="/WEB-INF/tlds/studBar" prefix="ict" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Student</title>
        <style>
        </style>
    </head>
    <body>
        <jsp:useBean scope="session" id="userInfo" class="ict.bean.StudentInfo"/>
        <ict:studBar name="<%=userInfo.getName()%>"/>
        <center><h1>Schedule</h1></center>
        <%
            ArrayList<LessonInfo> lesson = (ArrayList) request.getAttribute("LessonInfo");
            ArrayList<ArrayList<LessonInfo>> weekdayClass = new ArrayList();
            for (int i = 1; i <= 7; i++) {
                weekdayClass.add(new ArrayList());
            }
            int maxNum = 0;
            for (int i = 0; i < lesson.size(); i++) {
                int weekday;
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(lesson.get(i).getLessonDate());
                weekday = calendar.get(Calendar.DAY_OF_WEEK);
                weekdayClass.get(weekday - 1).add(lesson.get(i));
                if (weekdayClass.get(weekday - 1).size() > maxNum) {
                    maxNum = weekdayClass.get(weekday - 1).size();
                }
            }
        %>
        <center>
            <form action="student" method="get">
                <input type="hidden" name="action" value="schedule"/>
                <input type="hidden" name="search" value="date"/>
                <label>From : <input type="date" name="fDate" required/></label>
                <label>To : <input type="date" name="tDate" required/></label>
                <input type="submit" value="Serach"/>
            </form>
            <form action="student" method="get">
                <input type="hidden" name="action" value="schedule"/>
                <input type="submit" value="Cancel Serach"/>
            </form>
        </center>
        <table style="width: 100%" border="1">
            <tr>
                <th style="width: 14%">Sunday</th>
                <th style="width: 14%">Monday</th>
                <th style="width: 14%">Tuesday</th>
                <th style="width: 14%">Wednesday</th>
                <th style="width: 14%">Thursday</th>
                <th style="width: 14%">Friday</th>
                <th style="width: 14%">Saturday</th>
            </tr>
            <%
                if(maxNum == 0){
                    out.print("<tr><td colspan='7'><center><h1>Haven't any Lesson</h1></center></td></tr>");
                }
                for (int i = 0; i < maxNum; i++) {
            %>
            <tr>
                <%
                    for (int j = 0; j < 7; j++) {
                        if (weekdayClass.get(j).size() > i) {
                %>
                <td>
                    <table>
                        <tr>
                            <td>
                                Name:
                            </td>
                            <td>
                                <%=weekdayClass.get(j).get(i).getModuleID()%>
                                -
                                <%=weekdayClass.get(j).get(i).getModuleName()%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Teacher: 
                            </td>
                            <td>
                                <%=weekdayClass.get(j).get(i).getTeacherName()%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Date: 
                            </td>
                            <td>
                                <%
                                    SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
                                    out.print(ft.format(weekdayClass.get(j).get(i).getLessonDate()));
                                %>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Time: 
                            </td>
                            <td>
                                <%
                                    ft = new SimpleDateFormat("HH:mm");
                                    out.print(ft.format(weekdayClass.get(j).get(i).getLessonTime()));
                                %>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Duration: 
                            </td>
                            <td>
                                <%=weekdayClass.get(j).get(i).getLessonDuration()%> Hour(s)
                            </td>
                        </tr>
                    </table>
                </td>
                <%
                        } else {
                            out.println("<td><center>No Lesson Found</center></td>");
                        }
                    }
                %>
            </tr>
            <%
                }
            %>
        </table>
    </body>
</html>
