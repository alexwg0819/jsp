<%-- 
    Document   : studAttendent
    Created on : 2019/12/5, 下午 10:09:45
    Author     : tommylam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="ict.bean.*, java.util.*, java.text.*"%>
<%@ taglib uri="/WEB-INF/tlds/studBar" prefix="ict" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Module</title>
        <script>
            function attentend(moduleID){
                document.getElementById("form" + moduleID).submit();
            }
        </script>
    </head>
    <body>
        <jsp:useBean scope="session" id="userInfo" class="ict.bean.StudentInfo"/>
        <ict:studBar name="<%=userInfo.getName()%>"/>
        <center>
            <form action="student" method="get">
                Search : 
                <input type="hidden" name="action" value="module"/>
                <input type="hidden" name="search" value="search"/>
                <input type="text" name="value" style="width: 80%" placeholder="Search : "/>
                <input type="submit" value="Search" style="width: 10%"/>
            </form>
            <form action="student" method="get" id="cancelSearch">
                <input type="hidden" name="action" value="module"/>
                <input type="submit" value="Cancel Serach"/>
            </form>
            <table>
                <tr>
                    <%
                        ArrayList<LessonInfo> module = (ArrayList) request.getAttribute("Module");
                        if (module.size() == 0) {
                            out.print("<td>No Module Found</td>");
                        }
                        for (int i = 0; i < module.size(); i++) {
                            if (i % 3 == 0) {
                                out.print("</tr><tr>");
                            }
                    %>
                    <td>
                        <form action="student" method="get" id="form<%=module.get(i).getModuleID()%>">
                            <input type="hidden" name="action" value="attendent"/>
                            <input type="hidden" name="moduleID" value="<%=module.get(i).getModuleID()%>"/>
                        </form>
                        <button style="width: 600px; height: 200px; font-size: 20px" onclick="attentend('<%=module.get(i).getModuleID()%>')">
                            <center>
                                <table>
                                    <tr>
                                        <td>Module ID:</td>
                                        <td><%=module.get(i).getModuleID()%></td>
                                    </tr>
                                    <tr>
                                        <td>Module Name:</td>
                                        <td><%=module.get(i).getModuleName()%></td>
                                    </tr>
                                    <tr>
                                        <td>Teacher:</td>
                                        <td><%=module.get(i).getTeacherName()%></td>
                                    </tr>
                                </table>
                                <hr/>
                                <font style="font-weight: bold">Click to check Attendent</font>
                            </center>
                        </button>
                    </td>
                    <%}%>
                </tr>
            </table>
        </center>
    </body>
</html>
