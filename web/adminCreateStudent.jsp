<%-- 
    Document   : adminCreateUser
    Created on : 2019/12/13, 下午 07:49:05
    Author     : 85253
--%>

<%@page import="ict.bean.Program"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/adminBar" prefix="ict" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <script>
function myFunction() {
  var x = document.getElementById("mySelect").value;
  document.getElementById("program").value = x;
}
</script>
        <%  ArrayList<Program> programList = (ArrayList) request.getAttribute("ProductList");  %>
        
         <jsp:useBean scope="session" id="userInfo" class="ict.bean.AdminInfo"/>
        <ict:adminBar name="<%=userInfo.getName()%>"/>
    <center>
        <Form action="admin" method="get" id="regisitForm" >
            <fieldset>
                <legend> Create Studer Form : </legend>
                <label>StudentID : <input type="text" name="studentID"></input></label>
                <label>Password : <input type="text" name="password"></input></label>
                <label>StudentName : <input type="text" name="studentName"></input></label>
                <label>ProgramID :                 
                    <select id="mySelect" >
                        <% for(int i =0;i<programList.size();i++){%>
                        <option value="<%=programList.get(i).getProgramID() %>"><%=programList.get(i).getProgramID() %></option>
                        <%}%>
                    </select>
                    <input type="hidden" name="program" id="program">
                </label>
                <input type="hidden" name="action" value="selectClass">
                <button onclick="myFunction()" value="Submit">Next</button>
            </fieldset>
        </Form>
    </body>
</html>
