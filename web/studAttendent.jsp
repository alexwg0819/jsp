<%-- 
    Document   : studAttendent
    Created on : 2019/12/6, 下午 01:32:20
    Author     : tommylam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="ict.bean.*, java.util.*, java.text.*" %>
<%@ taglib uri="/WEB-INF/tlds/studBar" prefix="ict" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Attendent</title>
    </head>
    <body>
        <jsp:useBean scope="session" id="userInfo" class="ict.bean.StudentInfo"/>
        <ict:studBar name="<%=userInfo.getName()%>"/>
        <%
            ArrayList<Attendent> attendent = (ArrayList) request.getAttribute("Attendent");
        %>
        <h1>
            <table style="width: 100%">
                <tr>
                    <th>
                        Module ID : <%=attendent.get(0).getModuleID()%>
                    </th>
                    <th>
                        Module Name : <%=attendent.get(0).getModuleName()%>
                    </th>
                    <th>
                        Teacher : <%=attendent.get(0).getTeacherName()%>
                    </th>
                </tr>
            </table>
        </h1>
        <table style="width: 100%" border="1">
            <tr>
                <th>
                    Lesson
                </th>
                <th>
                    Duration
                </th>
                <th>
                    Date
                </th>
                <th>
                    Time
                </th>
                <th>
                    Attend Time
                </th>
            </tr>
            <%
                for (int i = 0; i < attendent.size(); i++) {
            %>
            <tr style="background-color: <%
                if(attendent.get(i).getAttendTime() != null){
                    if (attendent.get(i).getAttendTime().before(attendent.get(i).getLessonTime()) || attendent.get(i).getLessonTime().equals(attendent.get(i).getAttendTime())) {
                        out.print("#7FFFD4");
                    } else {
                        out.print("#D3D3D3");
                    }
                }else{
                    out.print("#F08080");
                }
                %>">
                <td align="center"><%=i + 1%></td>
                <td align="center"><%=attendent.get(i).getLessonDuration()%> Hour (s)</td>
                <td align="center"><%=new SimpleDateFormat("yyyy-MM-dd").format(attendent.get(i).getLessonDate())%></td>
                <td align="center"><%=new SimpleDateFormat("HH:mm").format(attendent.get(i).getLessonTime())%></td>
                <td align="center">
                    <%
                        if (attendent.get(i).getAttendTime() != null) {
                            out.print(new SimpleDateFormat("HH:mm").format(attendent.get(i).getAttendTime()));
                        } else {
                            out.print("Absent");
                        }
                    %>
                </td>
            </tr>
            <%}%>
        </table>
    </body>
</html>
