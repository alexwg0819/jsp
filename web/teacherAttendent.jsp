<%-- 
    Document   : teacherAttendent
    Created on : 2019/12/9, 下午 10:49:04
    Author     : tommylam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="ict.bean.*, java.util.*, java.text.*"%>
<%@ taglib uri="/WEB-INF/tlds/teacherBar" prefix="ict" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Attendent</title>
        <%
            ArrayList<AttendentList> attendentList = (ArrayList) request.getAttribute("attendentList");
            Calendar cal = Calendar.getInstance(); // creates calendar
            cal.setTime(attendentList.get(0).getLessonTime()); // sets calendar time/date
            cal.add(Calendar.HOUR_OF_DAY, 2); // adds one hour
        %>
        <script>
            function attendOnTime(attendTime) {
                document.getElementById(attendTime).value = "<%=new SimpleDateFormat("HH:mm").format(attendentList.get(0).getLessonTime())%>";
            }
            function absent(attendTime) {
                document.getElementById(attendTime).value = "";
            }
            function deleteLesson(){
                if(confirm("Are you sure to delete this lesson")){
                    document.getElementById("deleteLesson").submit();
                }
            }
        </script>
    </head>
    <body>
        <script>
            <%
                String message = (String) request.getAttribute("message");
                if (message != null) {
            %>
            alert("<%=message%>");
            <%
                }
            %>
        </script>
        <jsp:useBean scope="session" id="userInfo" class="ict.bean.TeacherInfo"/>
        <ict:teacherBar name="<%=userInfo.getName()%>"/>
        <h1>
            <table style="width: 100%">
                <tr>
                    <td>Class : </td>
                    <td><%=attendentList.get(0).getProgramName()%>-<%=attendentList.get(0).getClassID()%></td>
                    <td rowspan="4" align="center">
                        <form action="teacher" method="get" id="deleteLesson">
                            <input type="hidden" name="action" value="deleteLesson"/>
                            <input type="hidden" name="moduleID" value="<%=attendentList.get(0).getModuleID()%>"/>
                            <input type="hidden" name="programID" value="<%=attendentList.get(0).getProgramID()%>"/>
                            <input type="hidden" name="classID" value="<%=attendentList.get(0).getClassID()%>"/>
                            <input type="hidden" name="LessonID" value="<%=request.getParameter("LessonID")%>"/>
                        </form>
                        <button style="width: 300px; height: 200px; font-size: 70px; background-color: red" onclick="deleteLesson()">Delete Lesson</button>
                    </td>
                    <td rowspan="4" align="center"><button style="width: 300px; height: 200px; font-size: 100px" onclick="window.print();">Print</button></td>
                </tr>
                <tr>
                    <td>Module : </td>
                    <td><%=attendentList.get(0).getModuleName()%></td>
                </tr>
                <tr>
                    <td>Date : </td>
                    <td><%=new SimpleDateFormat("yyyy-MM-dd").format(attendentList.get(0).getLessonDate())%></td>
                </tr>
                <tr>
                    <td>Time : </td>
                    <td><%=new SimpleDateFormat("HH:mm").format(attendentList.get(0).getLessonTime())%>-<%=new SimpleDateFormat("HH:mm").format(attendentList.get(0).getLessonTime().getTime() + (int)(attendentList.get(0).getDuration() * 3600000))%></td>
                </tr>
            </table>
        </h1>
        <table style="width: 100%" border="1">
            <tr>
                <th>Student ID</th>
                <th>Student Name</th>
                <th>Attendent Time</th>
                <th>Set Attend on Time</th>
                <th>Set Absent</th>
                <th>Update</th>
            </tr>
            <%
                for (int i = 0; i < attendentList.size(); i++) {
            %>
            <form action="teacher" method="get">
                <input type="hidden" name="action" value="<%
                    if (attendentList.get(i).getAttendDate() != null) {
                        out.print("updateAttend");
                    } else {
                        out.print("createAttend");
                    }
                       %>"/>
                <input type="hidden" name="LessonID" value="<%=request.getParameter("LessonID")%>"/>
                <input type="hidden" name="studentID" value="<%=attendentList.get(i).getStudentID()%>"/>
                <tr style="background-color: <%
                    if (attendentList.get(i).getAttendDate() != null) {
                        if (attendentList.get(i).getAttendDate().before(attendentList.get(i).getLessonTime()) || attendentList.get(i).getLessonTime().equals(attendentList.get(i).getAttendDate())) {
                            out.print("#7FFFD4");
                        } else {
                            out.print("#D3D3D3");
                        }
                    } else {
                        out.print("#F08080");
                    }
                    %>">
                    <td align="center"><%=attendentList.get(i).getStudentID()%></td>
                    <td align="center"><%=attendentList.get(i).getStudentName()%></td>
                    <td align="center">
                        <input id="attendTime<%=attendentList.get(i).getStudentID()%>" type="time" name="attendTime"
                               min="<%=new SimpleDateFormat("HH:mm").format(attendentList.get(0).getLessonTime())%>"
                                   max="<%=new SimpleDateFormat("HH:mm").format(new Date(attendentList.get(0).getLessonTime().getTime() + (int)(attendentList.get(0).getDuration() * 3600000)))%>"
                               value="<%
                                   if (attendentList.get(i).getAttendDate() != null) {
                                       out.print(new SimpleDateFormat("HH:mm").format(attendentList.get(i).getAttendDate()));
                                   }
                               %>"<%
                                   if (attendentList.get(i).getAttendDate() == null) {
                                       out.print("required");
                                   }
                               %>/>
                    </td>
                    <td align="center">
                        <input type="button" onclick="attendOnTime('attendTime<%=attendentList.get(i).getStudentID()%>')" value="Attend on Time"/>
                    </td>
                    <td align="center">
                        <input type="button" onclick="absent('attendTime<%=attendentList.get(i).getStudentID()%>')" value="Absent"/>
                    </td>
                    <td align="center">
                        <input type="submit" value="Update"/>
                    </td>
                </tr>
            </form>
            <%
                }
            %>
        </table>
    </body>
</html>
