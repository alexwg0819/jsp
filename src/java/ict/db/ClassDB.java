/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ict.db;

import ict.bean.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author tommylam
 */
public class ClassDB {

    String dburl;
    String dbUser;
    String dbPassword;

    public Connection getConnection() throws SQLException, IOException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        return DriverManager.getConnection(dburl, dbUser, dbPassword);
    }

    public ClassDB(String dburl, String dbUser, String dbPassword) {
        this.dburl = dburl;
        this.dbUser = dbUser;
        this.dbPassword = dbPassword;
    }

    public ArrayList<LessonInfo> getLesson(String programID, String classID) {
        System.out.println(programID + ":" + classID);
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        ArrayList<LessonInfo> lesson = new ArrayList();
        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "SELECT `Module`.`ModuleName` AS 'ModuleName'\n"
                    + "	, `Module`.`ModuleID` AS 'ModuleID'\n"
                    + "    , `Teacher`.`TeacherName` AS 'TeacherName'\n"
                    + "    , `Lesson`.`Date` AS 'LessonDate'\n"
                    + "    , `Lesson`.`Time` AS 'LessonTime'\n"
                    + "    , `Lesson`.`Duration` AS 'LessonDuration'\n"
                    + "FROM `Class`, `Program_Module`, `Module`, `Teacher`, `Lesson` \n"
                    + "WHERE `Class`.`ProgramID` = `Program_Module`.`ProgramID` \n"
                    + "	AND	`Class`.`ClassID` = `Program_Module`.`ClassID`\n"
                    + "    AND `Module`.`ModuleID` = `Program_Module`.`ModuleID` \n"
                    + "    AND `Teacher`.`TeacherID` = `Program_Module`.`TeacherID`\n"
                    + "    AND `Program_Module`.`ProgramID` = `Lesson`.`ProgramID`\n"
                    + "    AND `Program_Module`.`ModuleID` = `Lesson`.`ModuleID`\n"
                    + "    AND `Class`.`ClassID` = `Lesson`.`ClassID`\n"
                    + "    AND `Program_Module`.`ProgramID` = ?\n"
                    + "    AND `Class`.`ClassID` = ?\n"
                    + "    AND `Lesson`.`Date` >= NOW()"
                    + "ORDER BY `LessonDate`  ASC\n"
                    + "	, `LessonTime`  ASC";
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            pStmnt.setString(1, programID);
            pStmnt.setString(2, classID);
            //4. execute the query and assign to the result
            ResultSet rs = pStmnt.executeQuery();
            String ModuleName, ModuleID, TeacherName;
            Date LessonDate, LessonTime;
            double LessonDuration;
            while (rs.next()) {
                ModuleName = rs.getString("ModuleName");
                ModuleID = rs.getString("ModuleID");
                TeacherName = rs.getString("TeacherName");
                LessonDate = new SimpleDateFormat("yyyy-MM-dd").parse(rs.getString("LessonDate"));
                LessonTime = new SimpleDateFormat("HH:mm:ss").parse(rs.getString("LessonTime"));
                LessonDuration = Double.parseDouble(rs.getString("LessonDuration"));
                lesson.add(new LessonInfo(ModuleName, ModuleID, TeacherName, LessonDate, LessonTime, LessonDuration));
            }
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return lesson;
    }

    public ArrayList<LessonInfo> getLessonByDate(String programID, String classID, String fDate, String tDate) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        ArrayList<LessonInfo> lesson = new ArrayList();
        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "SELECT `Module`.`ModuleName` AS 'ModuleName'\n"
                    + "	, `Module`.`ModuleID` AS 'ModuleID'\n"
                    + "    , `Teacher`.`TeacherName` AS 'TeacherName'\n"
                    + "    , `Lesson`.`Date` AS 'LessonDate'\n"
                    + "    , `Lesson`.`Time` AS 'LessonTime'\n"
                    + "    , `Lesson`.`Duration` AS 'LessonDuration'\n"
                    + "FROM `Class`, `Program_Module`, `Module`, `Teacher`, `Lesson` \n"
                    + "WHERE `Class`.`ProgramID` = `Program_Module`.`ProgramID` \n"
                    + "	AND	`Class`.`ClassID` = `Program_Module`.`ClassID`\n"
                    + "    AND `Module`.`ModuleID` = `Program_Module`.`ModuleID` \n"
                    + "    AND `Teacher`.`TeacherID` = `Program_Module`.`TeacherID`\n"
                    + "    AND `Program_Module`.`ProgramID` = `Lesson`.`ProgramID`\n"
                    + "    AND `Program_Module`.`ModuleID` = `Lesson`.`ModuleID`\n"
                    + "    AND `Class`.`ClassID` = `Lesson`.`ClassID`\n"
                    + "    AND `Program_Module`.`ProgramID` = ?\n"
                    + "    AND `Class`.`ClassID` = ?\n"
                    + "    AND `Lesson`.`Date` >= ?\n"
                    + "    AND `Lesson`.`Date` <= ?\n"
                    + "ORDER BY `LessonDate`  ASC\n"
                    + "	, `LessonTime`  ASC";
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            pStmnt.setString(1, programID);
            pStmnt.setString(2, classID);
            pStmnt.setString(3, fDate);
            pStmnt.setString(4, tDate);
            //4. execute the query and assign to the result
            ResultSet rs = pStmnt.executeQuery();
            String ModuleName, ModuleID, TeacherName;
            Date LessonDate, LessonTime;
            double LessonDuration;
            while (rs.next()) {
                ModuleName = rs.getString("ModuleName");
                ModuleID = rs.getString("ModuleID");
                TeacherName = rs.getString("TeacherName");
                LessonDate = new SimpleDateFormat("yyyy-MM-dd").parse(rs.getString("LessonDate"));
                LessonTime = new SimpleDateFormat("HH:mm:ss").parse(rs.getString("LessonTime"));
                LessonDuration = Double.parseDouble(rs.getString("LessonDuration"));
                lesson.add(new LessonInfo(ModuleName, ModuleID, TeacherName, LessonDate, LessonTime, LessonDuration));
            }
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return lesson;
    }

    public ArrayList<Attendent> getLessonByModule(String ProgramID, String ClassID, String ModuleID) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        ArrayList<Attendent> lesson = new ArrayList();
        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "SELECT `Lesson`.`Date` AS 'Date', `Lesson`.`Time` AS 'Time', `Lesson`.`LessionID` AS 'LessionID', `Lesson`.`Duration` AS 'Duration'\n"
                    + "FROM `Lesson`\n"
                    + "WHERE `Lesson`.`ModuleID` = ?\n"
                    + "	AND `Lesson`.`ClassID` = ?\n"
                    + "    AND `Lesson`.`ProgramID` = ?\n"
                    + "ORDER BY `Lesson`.`Date` ASC,\n"
                    + "	`Lesson`.`Time` ASC";
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            pStmnt.setString(1, ModuleID);
            pStmnt.setString(2, ClassID);
            pStmnt.setString(3, ProgramID);
            //4. execute the query and assign to the result
            ResultSet rs = pStmnt.executeQuery();
            Date LessonDate, LessonTime;
            double LessonDuration;
            while (rs.next()) {
                Attendent lessonInfo = new Attendent();
                lessonInfo.setLessonDate(new SimpleDateFormat("yyyy-MM-dd").parse(rs.getString("Date")));
                lessonInfo.setLessonTime(new SimpleDateFormat("HH:mm:ss").parse(rs.getString("Time")));
                lessonInfo.setLessonID(Integer.parseInt(rs.getString("LessionID")));
                lessonInfo.setLessonDuration(Double.parseDouble(rs.getString("Duration")));
                lesson.add(lessonInfo);
            }
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return lesson;
    }

    public ArrayList<Attendent> getLessonByModuleDate(String ProgramID, String ClassID, String ModuleID, String fDate, String tDate) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        ArrayList<Attendent> lesson = new ArrayList();
        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "SELECT `Lesson`.`Date` AS 'Date', `Lesson`.`Time` AS 'Time', `Lesson`.`LessionID` AS 'LessionID', `Lesson`.`Duration` AS 'Duration'\n"
                    + "FROM `Lesson`\n"
                    + "WHERE `Lesson`.`ModuleID` = ?\n"
                    + "	AND `Lesson`.`ClassID` = ?\n"
                    + "    AND `Lesson`.`ProgramID` = ?\n"
                    + "    AND `Lesson`.`Date` >= ?\n"
                    + "    AND `Lesson`.`Date` <= ?\n"
                    + "ORDER BY `Lesson`.`Date` ASC,\n"
                    + "	`Lesson`.`Time` ASC";
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            pStmnt.setString(1, ModuleID);
            pStmnt.setString(2, ClassID);
            pStmnt.setString(3, ProgramID);
            pStmnt.setString(4, fDate);
            pStmnt.setString(5, tDate);
            //4. execute the query and assign to the result
            ResultSet rs = pStmnt.executeQuery();
            Date LessonDate, LessonTime;
            double LessonDuration;
            while (rs.next()) {
                Attendent lessonInfo = new Attendent();
                lessonInfo.setLessonDate(new SimpleDateFormat("yyyy-MM-dd").parse(rs.getString("Date")));
                lessonInfo.setLessonTime(new SimpleDateFormat("HH:mm:ss").parse(rs.getString("Time")));
                lessonInfo.setLessonID(Integer.parseInt(rs.getString("LessionID")));
                lessonInfo.setLessonDuration(Double.parseDouble(rs.getString("Duration")));
                lesson.add(lessonInfo);
            }
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return lesson;
    }

    public ArrayList<LessonInfo> getModule(String ClassID, String ProgramID) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        ArrayList<LessonInfo> lesson = new ArrayList();
        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "SELECT	`Module`.`ModuleID` AS 'ModuleID', `Module`.`ModuleName` AS 'ModuleName', `Teacher`.`TeacherName` AS 'TeacherName'\n"
                    + "FROM	`Teacher`, `Program_Module`, `Module`\n"
                    + "WHERE `Program_Module`.`ModuleID` = `Module`.`ModuleID`\n"
                    + "    AND `Program_Module`.`TeacherID` = `Teacher`.`TeacherID`\n"
                    + "    AND `Program_Module`.`ClassID` = ?\n"
                    + "    AND `Program_Module`.`ProgramID` = ?";
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            pStmnt.setString(1, ClassID);
            pStmnt.setString(2, ProgramID);
            //4. execute the query and assign to the result
            ResultSet rs = pStmnt.executeQuery();
            while (rs.next()) {
                LessonInfo lessonInfo = new LessonInfo();
                lessonInfo.setModuleID(rs.getString("ModuleID"));
                lessonInfo.setModuleName(rs.getString("ModuleName"));
                lessonInfo.setTeacherName(rs.getString("TeacherName"));
                lesson.add(lessonInfo);
            }
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return lesson;
    }

    public ArrayList<LessonInfo> getModuleBySearch(String ClassID, String ProgramID, String value) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        ArrayList<LessonInfo> lesson = new ArrayList();
        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "SELECT	`Module`.`ModuleID` AS 'ModuleID', `Module`.`ModuleName` AS 'ModuleName', `Teacher`.`TeacherName` AS 'TeacherName'\n"
                    + "FROM	`Teacher`, `Program_Module`, `Module`\n"
                    + "WHERE `Program_Module`.`ModuleID` = `Module`.`ModuleID`\n"
                    + "    AND `Program_Module`.`TeacherID` = `Teacher`.`TeacherID`\n"
                    + "    AND `Program_Module`.`ClassID` = ?\n"
                    + "    AND `Program_Module`.`ProgramID` = ?"
                    + "    AND (`Teacher`.`TeacherName` LIKE '%" + value + "%'\n"
                    + "    	OR `Module`.`ModuleID` LIKE '%" + value + "%'\n"
                    + "        OR `Module`.`ModuleName` LIKE '%" + value + "%')";
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            pStmnt.setString(1, ClassID);
            pStmnt.setString(2, ProgramID);
            //4. execute the query and assign to the result
            ResultSet rs = pStmnt.executeQuery();
            while (rs.next()) {
                LessonInfo lessonInfo = new LessonInfo();
                lessonInfo.setModuleID(rs.getString("ModuleID"));
                lessonInfo.setModuleName(rs.getString("ModuleName"));
                lessonInfo.setTeacherName(rs.getString("TeacherName"));
                lesson.add(lessonInfo);
            }
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return lesson;
    }
        public ArrayList<Teacher> getTeatch() {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        ArrayList<Teacher> teacherInfo = new ArrayList();
        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "SELECT * FROM `teacher`";
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            //4. execute the query and assign to the result
            ResultSet rs = pStmnt.executeQuery();
            while (rs.next()) {
                Teacher Teacher = new Teacher(rs.getString("TeacherName"),rs.getString("TeacherID"),rs.getString("Permission"),rs.getString("Password"));

                teacherInfo.add(Teacher);
            }
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return teacherInfo;
    }
                public ArrayList<Student> getStudent() {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        ArrayList<Student> studentInfo = new ArrayList();
        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "SELECT * FROM `student` WHERE 1";
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            //4. execute the query and assign to the result
            ResultSet rs = pStmnt.executeQuery();
            while (rs.next()) {
                Student student = new Student(rs.getString("StudentID"),rs.getString("ClassID"),rs.getString("ProgramID"),rs.getString("StudentName"),rs.getString("Password"));

                studentInfo.add(student);
            }
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return studentInfo;
    }
               public ArrayList<Teacher> getTeatchBySearchRole(String role) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        ArrayList<Teacher> teacherInfo = new ArrayList();
        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "SELECT * FROM `teacher` Where `Permission` Like '%"+role+"%'";
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            //4. execute the query and assign to the result
            ResultSet rs = pStmnt.executeQuery();
            while (rs.next()) {
                Teacher Teacher = new Teacher(rs.getString("TeacherName"),rs.getString("TeacherID"),rs.getString("Permission"),rs.getString("Password"));

                teacherInfo.add(Teacher);
            }
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return teacherInfo;
    }
     public ArrayList<Teacher> getTeatchBySearchName(String Name) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        ArrayList<Teacher> teacherInfo = new ArrayList();
        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement =  "SELECT * FROM `teacher` Where `TeacherName` Like '%"+Name+"%'";
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            //4. execute the query and assign to the result
            ResultSet rs = pStmnt.executeQuery();
            while (rs.next()) {
                Teacher Teacher = new Teacher(rs.getString("TeacherName"),rs.getString("TeacherID"),rs.getString("Permission"),rs.getString("Password"));

                teacherInfo.add(Teacher);
            }
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return teacherInfo;
    }
       public ArrayList<Teacher> getTeatchBySearchID(String ID) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        ArrayList<Teacher> teacherInfo = new ArrayList();
        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "SELECT * FROM `teacher` Where `TeacherID` Like '%"+ID+"%'";
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            //4. execute the query and assign to the result
            ResultSet rs = pStmnt.executeQuery();
            while (rs.next()) {
                Teacher Teacher = new Teacher(rs.getString("TeacherName"),rs.getString("TeacherID"),rs.getString("Permission"),rs.getString("Password"));

                teacherInfo.add(Teacher);
            }
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return teacherInfo;
    }

    public ArrayList<LessonInfo> getModuleByTeacher(String TeacherID) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        ArrayList<LessonInfo> lesson = new ArrayList();
        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "SELECT	`Module`.`ModuleID` AS 'ModuleID', `Module`.`ModuleName` AS 'ModuleName'\n"
                    + "FROM	`Program_Module`, `Module`\n"
                    + "WHERE `Program_Module`.`ModuleID` = `Module`.`ModuleID`\n"
                    + "    AND `Program_Module`.`TeacherID` = ?\n"
                    + "GROUP BY `Program_Module`.`ModuleID`";
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            pStmnt.setString(1, TeacherID);
            //4. execute the query and assign to the result
            ResultSet rs = pStmnt.executeQuery();
            while (rs.next()) {
                LessonInfo lessonInfo = new LessonInfo();
                lessonInfo.setModuleID(rs.getString("ModuleID"));
                lessonInfo.setModuleName(rs.getString("ModuleName"));
                lesson.add(lessonInfo);
            }
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return lesson;
    }

    public ArrayList<LessonInfo> getModuleByTeacherSearch(String TeacherID, String value) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        ArrayList<LessonInfo> lesson = new ArrayList();
        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "SELECT	`Module`.`ModuleID` AS 'ModuleID', `Module`.`ModuleName` AS 'ModuleName'\n"
                    + "FROM	`Program_Module`, `Module`\n"
                    + "WHERE `Program_Module`.`ModuleID` = `Module`.`ModuleID`\n"
                    + "    AND `Program_Module`.`TeacherID` = ?\n"
                    + "    AND (`Program_Module`.`ProgramID` LIKE '%" + value + "%'\n"
                    + "    	OR `Program_Module`.`ClassID` LIKE '%" + value + "%'\n"
                    + "    	OR `Module`.`ModuleID` LIKE '%" + value + "%'\n"
                    + "        OR `Module`.`ModuleName` LIKE '%" + value + "%')\n"
                    + "GROUP BY `Program_Module`.`ModuleID`";
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            pStmnt.setString(1, TeacherID);
            //4. execute the query and assign to the result
            ResultSet rs = pStmnt.executeQuery();
            while (rs.next()) {
                LessonInfo lessonInfo = new LessonInfo();
                lessonInfo.setModuleID(rs.getString("ModuleID"));
                lessonInfo.setModuleName(rs.getString("ModuleName"));
                lesson.add(lessonInfo);
            }
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return lesson;
    }

    public ArrayList<Attendent> getAttendent(String ClassID, String ProgramID, String ModuleID, String StudentID) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        ArrayList<Attendent> attendent = new ArrayList();
        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "SELECT `Module`.`ModuleName` AS 'ModuleName'\n"
                    + "	, `Module`.`ModuleID` AS 'ModuleID'\n"
                    + "    , `Teacher`.`TeacherName` AS 'TeacherName'\n"
                    + "    , `Lesson`.`LessionID` AS 'LessionID'\n"
                    + "    , `Lesson`.`Date` AS 'LessonDate'\n"
                    + "    , `Lesson`.`Time` AS 'LessonTime'\n"
                    + "    , `Lesson`.`Duration` AS 'LessonDuration'\n"
                    + "FROM `Class`, `Program_Module`, `Module`, `Teacher`, `Lesson` \n"
                    + "WHERE `Class`.`ProgramID` = `Program_Module`.`ProgramID` \n"
                    + "	AND	`Class`.`ClassID` = `Program_Module`.`ClassID`\n"
                    + "    AND `Module`.`ModuleID` = `Program_Module`.`ModuleID` \n"
                    + "    AND `Teacher`.`TeacherID` = `Program_Module`.`TeacherID`\n"
                    + "    AND `Program_Module`.`ProgramID` = `Lesson`.`ProgramID`\n"
                    + "    AND `Program_Module`.`ModuleID` = `Lesson`.`ModuleID`\n"
                    + "    AND `Class`.`ClassID` = `Lesson`.`ClassID`\n"
                    + "    AND `Program_Module`.`ProgramID` = ?\n"
                    + "    AND `Class`.`ClassID` = ?\n"
                    + "    AND `Program_Module`.`ModuleID` = ?\n"
                    + "    AND `Lesson`.`Date` < NOW()\n"
                    + "ORDER BY `LessonDate`  ASC\n"
                    + "	, `LessonTime`  ASC";
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            pStmnt.setString(1, ProgramID);
            pStmnt.setString(2, ClassID);
            pStmnt.setString(3, ModuleID);
            //4. execute the query and assign to the result
            ResultSet rs = pStmnt.executeQuery();
            String ModuleName, TeacherName;
            int LessonID;
            Date LessonDate;
            Date LessonTime;
            double LessonDuration;
            while (rs.next()) {
                ModuleName = rs.getString("ModuleName");
                TeacherName = rs.getString("TeacherName");
                LessonID = Integer.parseInt(rs.getString("LessionID"));
                LessonDate = new SimpleDateFormat("yyyy-MM-dd").parse(rs.getString("LessonDate"));
                LessonTime = new SimpleDateFormat("HH:mm:ss").parse(rs.getString("LessonTime"));
                LessonDuration = Double.parseDouble(rs.getString("LessonDuration"));
                attendent.add(new Attendent(getAttendTime(StudentID, LessonID), LessonID, ModuleName, ModuleID, TeacherName, LessonDate, LessonTime, LessonDuration));
            }
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return attendent;
    }
    

    public Date getAttendTime(String StudentID, int LessonID) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        Date AttendTime = null;
        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "SELECT `Attendent`.`AttendTime` AS 'AttendTime'\n"
                    + "FROM `Attendent`\n"
                    + "WHERE `Attendent`.`StudentID` = ?\n"
                    + "	AND `Attendent`.`LessionID` = ?";
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            pStmnt.setString(1, StudentID);
            pStmnt.setString(2, Integer.toString(LessonID));
            //4. execute the query and assign to the result
            ResultSet rs = pStmnt.executeQuery();
            while (rs.next()) {
                AttendTime = new SimpleDateFormat("HH:mm:ss").parse(rs.getString("AttendTime"));
            }
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return AttendTime;
    }

    public ArrayList<ClassInfo> getClass(String TeacherID, String ModuleID) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        ArrayList<ClassInfo> classInfo = new ArrayList();
        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "SELECT `Program_Module`.`ClassID` AS 'ClassID', `Program`.`ProgramName` AS 'ProgramName', `Program`.`ProgramID` AS 'ProgramID'\n"
                    + "FROM `Program`, `Program_Module`\n"
                    + "WHERE `Program_Module`.`ProgramID` = `Program`.`ProgramID`\n"
                    + "    AND `Program_Module`.`TeacherID` = ?\n"
                    + "    AND `Program_Module`.`ModuleID` = ?\n"
                    + "GROUP BY `Program_Module`.`ProgramID`, `Program_Module`.`ClassID`\n"
                    + "ORDER BY `Program_Module`.`ProgramID` ASC,\n"
                    + "	`Program_Module`.`ClassID` ASC";
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            pStmnt.setString(1, TeacherID);
            pStmnt.setString(2, ModuleID);
            //4. execute the query and assign to the result
            ResultSet rs = pStmnt.executeQuery();
            String ClassID, ProgramName, ProgramID;
            while (rs.next()) {
                ClassID = rs.getString("ClassID");
                ProgramName = rs.getString("ProgramName");
                ProgramID = rs.getString("ProgramID");
                classInfo.add(new ClassInfo(ClassID, ProgramName, ProgramID, ModuleID));
            }
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return classInfo;
    }

    public ArrayList<ClassInfo> getClassBySearch(String TeacherID, String ModuleID, String value) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        ArrayList<ClassInfo> classInfo = new ArrayList();
        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "SELECT `Class`.`ClassID` AS 'ClassID', `Program`.`ProgramName` AS 'ProgramName', `Program`.`ProgramID` AS 'ProgramID'\n"
                    + "FROM `Class`, `Program`, `Program_Module`\n"
                    + "WHERE `Program_Module`.`ClassID` = `Class`.`ClassID`\n"
                    + "	AND `Program_Module`.`ProgramID` = `Program`.`ProgramID` = `Class`.`ProgramID`\n"
                    + "    AND `Program_Module`.`TeacherID` = ?\n"
                    + "    AND `Program_Module`.`ModuleID` = ?  \n"
                    + "    AND (`Program_Module`.`ClassID` LIKE '%" + value + "%'\n"
                    + "         OR `Program`.`ProgramID` LIKE '%" + value + "%'\n"
                    + "         OR `Program`.`ProgramName` LIKE '%" + value + "%')\n"
                    + "GROUP BY `Class`.`ClassID`, `Program`.`ProgramID`"
                    + "ORDER BY `Program`.`ProgramID` ASC,\n"
                    + "	`Class`.`ClassID` ASC";
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            pStmnt.setString(1, TeacherID);
            pStmnt.setString(2, ModuleID);
            //4. execute the query and assign to the result
            ResultSet rs = pStmnt.executeQuery();
            String ClassID, ProgramName, ProgramID;
            while (rs.next()) {
                ClassID = rs.getString("ClassID");
                ProgramName = rs.getString("ProgramName");
                ProgramID = rs.getString("ProgramID");
                classInfo.add(new ClassInfo(ClassID, ProgramName, ProgramID, ModuleID));
            }
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return classInfo;
    }

public ArrayList<AttendentList> getAttendentList(int lessonID) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        ArrayList<AttendentList> attendentList = new ArrayList();
        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "SELECT `Program`.`ProgramName` AS 'ProgramName', `Program`.`ProgramID` AS 'ProgramID', `Program_Module`.`ClassID` AS 'ClassID', `Module`.`ModuleName` AS 'ModuleName', "
                    + "    `Module`.`ModuleID` AS 'ModuleID', `Lesson`.`Date` AS 'Date', `Lesson`.`Time` AS 'Time', `Student`.`StudentID` AS 'StudentID', `Student`.`StudentName` AS 'StudentName', `Lesson`.`Duration` AS 'Duration'\n"
                    + "FROM `Program_Module`, `Lesson`, `Class`, `Student`, `Module`, `Program`\n"
                    + "WHERE `Program_Module`.`ClassID` = `Lesson`.`ClassID`\n"
                    + "	AND `Program_Module`.`ClassID` = `Class`.`ClassID`\n"
                    + "    AND `Class`.`ClassID` = `Student`.`ClassID`\n"
                    + "	AND `Program_Module`.`ProgramID` = `Lesson`.`ProgramID`\n"
                    + "    AND `Program_Module`.`ProgramID` = `Class`.`ProgramID`\n"
                    + "    AND `Class`.`ProgramID` = `Program`.`ProgramID`\n"
                    + "    AND `Class`.`ProgramID` = `Student`.`ProgramID`\n"
                    + "    AND `Program_Module`.`ModuleID` = `Lesson`.`ModuleID`\n"
                    + "    AND `Program_Module`.`ModuleID` = `Module`.`ModuleID`\n"
                    + "    AND `Lesson`.`LessionID` = ?\n"
                    + "ORDER BY `Student`.`StudentID` ASC";
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            pStmnt.setString(1, Integer.toString(lessonID));
            //4. execute the query and assign to the result
            ResultSet rs = pStmnt.executeQuery();
            String ProgramName, ProgramID, ClassID, ModuleName, ModuleID, StudentName;
            Date lessonDate, lessonTime, attendDate;
            int StudentID;
            double Duration;
            while (rs.next()) {
                ProgramName = rs.getString("ProgramName");
                ProgramID = rs.getString("ProgramID");
                ClassID = rs.getString("ClassID");
                ModuleName = rs.getString("ModuleName");
                ModuleID = rs.getString("ModuleID");
                StudentName = rs.getString("StudentName");
                lessonDate = new SimpleDateFormat("yyyy-MM-dd").parse(rs.getString("Date"));
                lessonTime = new SimpleDateFormat("HH:mm:ss").parse(rs.getString("Time"));
                StudentID = Integer.parseInt(rs.getString("StudentID"));
                attendDate = getAttendTime(Integer.toString(StudentID), lessonID);
                Duration = Double.parseDouble(rs.getString("Duration"));
                attendentList.add(new AttendentList(ProgramName, ProgramID, ClassID, ModuleName, ModuleID, StudentName, lessonDate, lessonTime, attendDate, StudentID, Duration));
            }
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return attendentList;
    }
       public ArrayList<Report> StudentList(String ModuleID1,String ClassID1, String ProgramID1) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        ArrayList<Report> attendentList = new ArrayList();
        
        
        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "SELECT `StudentID`,`StudentName` FROM `student` WHERE `ProgramID` =? AND `ClassID` =?";
            
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            pStmnt.setString(1, ProgramID1);
            pStmnt.setString(2, ClassID1);
            //4. execute the query and assign to the result
            ResultSet rs = pStmnt.executeQuery();
            String  stuID, stuName;
            Date lessonDate, lessonTime, attendDate;
            while (rs.next()) {
                stuName = rs.getString("StudentName");
                stuID = rs.getString("StudentID");
                ArrayList<Attendent> attendent = getAttendent(ClassID1, ProgramID1, ModuleID1, stuID);
                attendentList.add(new Report(stuName,stuID,ClassID1,ProgramID1,ModuleID1,attendent));
            }
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return attendentList;
    }
       public ArrayList<Program> getProgram() {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        ArrayList<Program> attendentList = new ArrayList();
        
        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "SELECT DISTINCT `ProgramID` FROM `program` WHERE 1";
            
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            
            //3. update the placeholders with username and pwd
            //4. execute the query and assign to the result
            ResultSet rs = pStmnt.executeQuery();
            String  programID;
            Date lessonDate, lessonTime, attendDate;
            while (rs.next()) {
                programID = rs.getString("ProgramID");
 
                ArrayList attendent = getProgramClass(programID);
                attendentList.add(new Program(attendent,programID));
            }
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return attendentList;
    }
        public ArrayList getProgramClass(String ProgramID) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        ArrayList classList = new ArrayList();
        
        try {
            //1. get Connection
            
            cnnct = getConnection();
            String preQueryStatement = "SELECT DISTINCT `ClassID` FROM `program_module` WHERE `ProgramID` =? ";
            
            
            
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            pStmnt.setString(1, ProgramID);
            //3. update the placeholders with username and pwd
            //4. execute the query and assign to the result
            ResultSet rs = pStmnt.executeQuery();
            String  classID;
            Date lessonDate, lessonTime, attendDate;
            while (rs.next()) {
                System.out.print("ok");
                classID = rs.getString("ClassID");
                classList.add(classID);
            }
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return classList;
    }
       
    public boolean deleteAttendent(int lessonID, int StudentID) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        boolean inSuccess = false;
        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "DELETE FROM `Attendent` WHERE `Attendent`.`LessionID` = ? AND `Attendent`.`StudentID` = ?";
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            pStmnt.setString(1, Integer.toString(lessonID));
            pStmnt.setString(2, Integer.toString(StudentID));
            //4. execute the query and assign to the result
            int rowCount = pStmnt.executeUpdate();
            if (rowCount >= 1) {
                inSuccess = true;
            }
            pStmnt.close();
            cnnct.close();
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return inSuccess;
    }
        public boolean addStudent(String stuID, String ClassID,String ProgramID,String StudentName,String Password) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        boolean inSuccess = false;
        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "INSERT INTO `student` (`StudentID`, `ClassID`, `ProgramID`, `StudentName`, `Password`) VALUES (?, ?, ?, ?, ?)";
            
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            pStmnt.setString(1, stuID);
            pStmnt.setString(2, ClassID);
            pStmnt.setString(3, ProgramID);
            pStmnt.setString(4, StudentName);
            pStmnt.setString(5, Password);
            //4. execute the query and assign to the result
            int rowCount = pStmnt.executeUpdate();
            if (rowCount >= 1) {
                inSuccess = true;
            }
            pStmnt.close();
            cnnct.close();
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return inSuccess;
    }
                public boolean addTeacher(String teacherID, String permisson,String teacherName,String Password) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        boolean inSuccess = false;
        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "INSERT INTO `teacher` (`TeacherID`, `TeacherName`, `Password`, `Permission`) VALUES (?, ?, ?, ?)";
            
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            pStmnt.setString(1, teacherID);
            pStmnt.setString(2, teacherName);
            pStmnt.setString(3, Password);
            pStmnt.setString(4, permisson);
            //4. execute the query and assign to the result
            int rowCount = pStmnt.executeUpdate();
            if (rowCount >= 1) {
                inSuccess = true;
            }
            pStmnt.close();
            cnnct.close();
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return inSuccess;
    }

    public boolean updateAttendent(int lessonID, int StudentID, String AttendTime) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        boolean inSuccess = false;
        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "UPDATE `Attendent` SET `Attendent`.`AttendTime` = ? WHERE `Attendent`.`LessionID` = ? AND `Attendent`.`StudentID` = ?";
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            pStmnt.setString(1, AttendTime);
            pStmnt.setString(2, Integer.toString(lessonID));
            pStmnt.setString(3, Integer.toString(StudentID));
            //4. execute the query and assign to the result
            int rowCount = pStmnt.executeUpdate();
            if (rowCount >= 1) {
                inSuccess = true;
            }
            pStmnt.close();
            cnnct.close();
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return inSuccess;
    }

    public boolean createAttendent(int lessonID, int StudentID, String AttendTime) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        boolean inSuccess = false;
        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "INSERT INTO `Attendent` (`StudentID`, `LessionID`, `AttendTime`) VALUES (?, ?, ?)";
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            pStmnt.setString(1, Integer.toString(StudentID));
            pStmnt.setString(2, Integer.toString(lessonID));
            pStmnt.setString(3, AttendTime);
            //4. execute the query and assign to the result
            int rowCount = pStmnt.executeUpdate();
            if (rowCount >= 1) {
                inSuccess = true;
            }
            pStmnt.close();
            cnnct.close();
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return inSuccess;
    }

    public boolean createLesson(String ModuleID, String ClassID, String ProgramID, String Date, String Time, Double Duration) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        boolean inSuccess = false;
        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "INSERT INTO `Lesson` (`LessionID`, `ModuleID`, `ClassID`, `ProgramID`, `Date`, `Time`, `Duration`) VALUES ( NULL, ?, ?, ?, ?, ?, ?)";
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            pStmnt.setString(1, ModuleID);
            pStmnt.setString(2, ClassID);
            pStmnt.setString(3, ProgramID);
            pStmnt.setString(4, Date);
            pStmnt.setString(5, Time);
            pStmnt.setString(6, Double.toString(Duration));
            //4. execute the query and assign to the result
            int rowCount = pStmnt.executeUpdate();
            if (rowCount >= 1) {
                inSuccess = true;
            }
            pStmnt.close();
            cnnct.close();
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return inSuccess;
    }

    public boolean deleteLesson(int lessonID) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        boolean inSuccess = false;
        boolean inSuccess2 = false;
        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "DELETE FROM `Attendent` WHERE `LessionID` = " + lessonID + ";";
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            //4. execute the query and assign to the result
            int rowCount = pStmnt.executeUpdate();
            if (rowCount >= 1) {
                inSuccess = true;
            }
            pStmnt.close();
            cnnct.close();
            
            //1. get Connection
            cnnct = getConnection();
            preQueryStatement = "DELETE FROM `Lesson` WHERE `LessionID` = " + lessonID + ";";
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            //4. execute the query and assign to the result
            int rowCount2 = pStmnt.executeUpdate();
            if (rowCount2 >= 1) {
                inSuccess2 = true;
            }
            pStmnt.close();
            cnnct.close();
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return inSuccess2;
    }
        public boolean changeTeacherRole(String Id,String role) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        boolean inSuccess = false;

        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "UPDATE `teacher` SET `Permission`="+role+" WHERE `TeacherID` = "+Id;
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            //4. execute the query and assign to the result
            int rowCount = pStmnt.executeUpdate();
            if (rowCount >= 1) {
                inSuccess = true;
            }
            pStmnt.close();
            cnnct.close();
            
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return inSuccess;
    }
                public boolean changeStudent(String Id,String name,String password) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        boolean inSuccess = false;

        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "UPDATE `student` SET `StudentName`=\""+name+"\",`Password`= \""+password+"\"WHERE `StudentID` ="+Id;
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            //4. execute the query and assign to the result
            int rowCount = pStmnt.executeUpdate();
            if (rowCount >= 1) {
                inSuccess = true;
            }
            pStmnt.close();
            cnnct.close();
            
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return inSuccess;
    }
      public ArrayList<Student> getStudentBySearchID(String ID) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        ArrayList<Student> studentInfo = new ArrayList();
        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "SELECT * FROM `student` WHERE `StudentID` LIKE\"%"+ID+"%\"";
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            //4. execute the query and assign to the result
            ResultSet rs = pStmnt.executeQuery();
            while (rs.next()) {
                Student student = new Student(rs.getString("StudentID"),rs.getString("ClassID"),rs.getString("ProgramID"),rs.getString("StudentName"),rs.getString("Password"));

                studentInfo.add(student);
            }
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return studentInfo;
    }
            public ArrayList<Student> getStudentBySearchName(String Name) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        ArrayList<Student> studentInfo = new ArrayList();
        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "SELECT * FROM `student` WHERE `StudentName` LIKE \"%"+Name+"%\"";
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            //4. execute the query and assign to the result
            ResultSet rs = pStmnt.executeQuery();
            while (rs.next()) {
                Student student = new Student(rs.getString("StudentID"),rs.getString("ClassID"),rs.getString("ProgramID"),rs.getString("StudentName"),rs.getString("Password"));

                studentInfo.add(student);
            }
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return studentInfo;
    }
                        public ArrayList<Student> getStudentBySearchClassID(String ClassID) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        ArrayList<Student> studentInfo = new ArrayList();
        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "SELECT * FROM `student` WHERE `ClassID` LIKE \"%"+ClassID+"%\"";
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            //4. execute the query and assign to the result
            ResultSet rs = pStmnt.executeQuery();
            while (rs.next()) {
                Student student = new Student(rs.getString("StudentID"),rs.getString("ClassID"),rs.getString("ProgramID"),rs.getString("StudentName"),rs.getString("Password"));

                studentInfo.add(student);
            }
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return studentInfo;
    }
                                                public ArrayList<Student> getStudentBySearchProgramID(String ProgramID) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        ArrayList<Student> studentInfo = new ArrayList();
        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "SELECT * FROM `student` WHERE `ProgramID` LIKE\"%"+ProgramID+"%\"";
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            //4. execute the query and assign to the result
            ResultSet rs = pStmnt.executeQuery();
            while (rs.next()) {
                Student student = new Student(rs.getString("StudentID"),rs.getString("ClassID"),rs.getString("ProgramID"),rs.getString("StudentName"),rs.getString("Password"));

                studentInfo.add(student);
            }
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return studentInfo;
    }
                                                                public boolean changeTeacher(String Id,String name,String password,String role) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        boolean inSuccess = false;

        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = " UPDATE `teacher` SET `TeacherName`=\""+name+"\",`Password`= \""+password+"\",`Permission`=\""+role+"\"WHERE `TeacherID` ="+Id;
          
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            //4. execute the query and assign to the result
            int rowCount = pStmnt.executeUpdate();
            if (rowCount >= 1) {
                inSuccess = true;
            }
            pStmnt.close();
            cnnct.close();
            
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return inSuccess;
    }
      public ArrayList<Admin> getAdmin(String ID) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        ArrayList<Admin> studentInfo = new ArrayList();
        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "SELECT * FROM `admin` WHERE `AdminID` = "+ID;
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            //4. execute the query and assign to the result
            ResultSet rs = pStmnt.executeQuery();
            while (rs.next()) {
                Admin student = new Admin(rs.getString("AdminName"),rs.getString("AdminID"),rs.getString("Password"));
                System.out.println(rs.getString("AdminName"));
                studentInfo.add(student);
            }
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return studentInfo;
    }
                      public boolean changeProfile(String Id,String name,String password) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        boolean inSuccess = false;

        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "UPDATE `admin` SET`AdminName`=\""+name+"\",`Password`=\""+password+"\"WHERE `AdminID` = "+Id;
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            //4. execute the query and assign to the result
            int rowCount = pStmnt.executeUpdate();
            if (rowCount >= 1) {
                inSuccess = true;
            }
            pStmnt.close();
            cnnct.close();
            
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return inSuccess;
    }
}
