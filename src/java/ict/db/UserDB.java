/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ict.db;

import ict.bean.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author tommylam
 */
public class UserDB {

    String dburl;
    String dbUser;
    String dbPassword;

    public Connection getConnection() throws SQLException, IOException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        return DriverManager.getConnection(dburl, dbUser, dbPassword);
    }

    public UserDB(String dburl, String dbUser, String dbPassword) {
        this.dburl = dburl;
        this.dbUser = dbUser;
        this.dbPassword = dbPassword;
    }

    public boolean isValidUser(String id, String pwd, String role) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
//        CustomerBean cb = null;
        boolean isValid = false;
        try {
            //1. get Connection
            cnnct = getConnection();
            String preQueryStatement = "SELECT * FROM " + role + " WHERE " + role + "ID =  ? and  password =  ?";
            //2. get the prepare Statement
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            //3. update the placeholders with username and pwd
            pStmnt.setString(1, id);
            pStmnt.setString(2, pwd);
            //4. execute the query and assign to the result
            ResultSet rs = pStmnt.executeQuery();
            if (rs.next()) {
                isValid = true;
            }
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return isValid;
    }

    public UserInfo getUserInfo(String id, String role) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        UserInfo userInfo = null;
        try {
            cnnct = getConnection();
            String preQueryStatement = "select * from " + role + " WHERE " + role + "ID=?";
            pStmnt = cnnct.prepareStatement(preQueryStatement);
            pStmnt.setString(1, id);
            ResultSet rs = pStmnt.executeQuery();
            if (rs.next()) {
                int userID = Integer.parseInt(id);
                String name = rs.getString(role + "Name");
                if ("Student".equals(role)) {
                    String programID = rs.getString("ProgramID");
                    String classID = rs.getString("ClassID");
                    userInfo = new StudentInfo(programID, classID, userID, name, role);
                } else if ("Teacher".equals(role)) {
                    int permission = Integer.parseInt(rs.getString("Permission"));
                    userInfo = new TeacherInfo(permission, userID, name, role);
                } else if ("Admin".equals(role)) {
                    userInfo = new AdminInfo(userID, name, role);
                }
            }
            pStmnt.close();
            cnnct.close();
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return userInfo;
    }

}
