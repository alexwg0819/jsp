/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ict.tag;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 *
 * @author tommylam
 */
public class adminBar extends SimpleTagSupport {

    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void doTag() {
        try {
            PageContext pageContext = (PageContext) getJspContext();
            JspWriter out = pageContext.getOut();
            out.println("<div style=\"padding: 0px; margin: 0px; border-style: solid; border-color: black; background-color: lightgray\">\n"
                    + "            <table style=\"height: 50px; width: 100%\">\n"
                    + "                <tr style=\"height: 100%\">\n"
                    + "                    <td style=\"height: 100%\">\n"
                    + "                        <table style=\"height: 100%\">\n"
                    + "                            <tr style=\"height: 100%\">\n"
                    + addTag("admin", "createUserStudent", "Create Student Account")
                    + addTag("admin", "createUserTeacher", "Create Teacher Account")
                    + addTag("admin", "modifityStudent", "Modifity Student Profile")
                    + addTag("admin", "modifityTeacher", "Modifity Teacher Profile")
                    + addTag("admin", "changeRoleStep1", "Change Role")
                    + addTag("admin", "modifityProfile", "Change Profile")
                    + "                            </tr>\n"
                    + "                        </table>\n"
                    + "                    </td>\n"
                    + "                    <td align=\"right\" style=\"height: 100%\">\n"
                    + "                        <form action=\"main\" method=\"get\" id=\"logout\">\n"
                    + "                            <input type=\"hidden\" name=\"action\" value=\"logout\"/>\n"
                    + "                        </form>\n"
                    + "Welcome, " + name + "!"
                    + "                        <button style=\"height: 100%; width: 100px; background-color: lightblue\" align=\"center\" onclick=\"document.getElementById('logout').submit();\">Logout</button>\n"
                    + "                    </td>\n"
                    + "                </tr>\n"
                    + "            </table>\n"
                    + "        </div>");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String addTag(String path, String action, String text) {
        return "<td align=\"center\">\n"
                + "   <form action=\"" + path + "\" method=\"get\" id=\"to" + action + "\">\n"
                + "       <input type=\"hidden\" name=\"action\" value=\"" + action + "\"/>\n"
                + "   </form>\n"
                + "   <button style=\"height: 100%; width: 100px; background-color: lightblue\" align=\"center\" onclick=\"document.getElementById('to" + action + "').submit();\">" + text + "</button>\n"
                + "</td>";
    }
}
