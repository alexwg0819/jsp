/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ict.servlet;

import ict.bean.Admin;
import ict.bean.AdminInfo;
import ict.bean.Attendent;
import ict.bean.LessonInfo;
import ict.bean.Program;
import ict.bean.Student;
import ict.bean.StudentInfo;
import ict.bean.Teacher;
import ict.db.ClassDB;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author 85253
 */
@WebServlet(name = "HandleAdmin", urlPatterns = {"/admin"})
public class HandleAdmin extends HttpServlet {
    private ClassDB db;
      public void init() {
        String dbUser = this.getServletContext().getInitParameter("dbUser");
        String dbPassword = this.getServletContext().getInitParameter("dbPassword");
        String dbUrl = this.getServletContext().getInitParameter("dbUrl");
        db = new ClassDB(dbUrl, dbUser, dbPassword);
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        String targetURL = "";
        if ("createUserStudent".equalsIgnoreCase(action)) {
            ArrayList<Program> programList = getProgram(request,response);
            request.setAttribute("ProductList", programList);
            targetURL = "adminCreateStudent.jsp";
        }else if("createUserTeacher".equalsIgnoreCase(action)) {
            targetURL = "adminCreateTeacher.jsp";
        }else if("selectClass".equalsIgnoreCase(action)) {
            ArrayList classList = getProgramClass(request,response);
            request.setAttribute("ClassList", classList);
            targetURL = "CreateStudentSelectClass.jsp";
            
        }else if("lastRegisit".equalsIgnoreCase(action)) {
            if(addUser(request,response) == true){
            request.setAttribute("check","Add Student Successful" );
            }else{
            request.setAttribute("check","Add Student Failed,Try Again" );
            }
            targetURL = "admin.jsp";
        } else if("createteacherfin".equalsIgnoreCase(action)) {
            if(addTeacher(request,response) == true){
            request.setAttribute("check","Add teacher Successful" );
            }else{
            request.setAttribute("check","Add teacher Failed,Try Again" );
            }
            targetURL = "admin.jsp";
        }else if("changeRoleStep1".equalsIgnoreCase(action)) {
            
            ArrayList TeacherList = getTeacher(request,response);
            if(request.getParameter("search")!= null){
                
                if(request.getParameter("type").equals( "1")){     
                TeacherList = db.getTeatchBySearchID(request.getParameter("search"));
                }else if(request.getParameter("type").equals("2")){
                TeacherList = db.getTeatchBySearchName(request.getParameter("search"));
                }else if(request.getParameter("type").equals("3")){
                TeacherList = db.getTeatchBySearchRole(request.getParameter("search"));
                }
            }
            
            request.setAttribute("TeacherList", TeacherList);
            targetURL = "adminChageRole.jsp";
        }else if("changeRolefin".equalsIgnoreCase(action)) {
            if(changeRole(request,response)==true){
            request.setAttribute("check","Change Successful" );
            }else{
            request.setAttribute("check","Change is not Successful, Please try again" );
            }
            ArrayList TeacherList = getTeacher(request,response);
            request.setAttribute("TeacherList", TeacherList);
            targetURL = "adminChageRole.jsp";
        }else if("modifityStudent".equalsIgnoreCase(action)) {
            ArrayList<Student> Student = getStudent(request,response);
                if(request.getParameter("search")!= null){
                
                if(request.getParameter("type").equals( "1")){     
                Student = db.getStudentBySearchID(request.getParameter("search"));
                }else if(request.getParameter("type").equals("2")){
                Student = db.getStudentBySearchName(request.getParameter("search"));
                }else if(request.getParameter("type").equals("4")){
                Student = db.getStudentBySearchProgramID(request.getParameter("search"));
                }else if(request.getParameter("type").equals("3")){
                Student = db.getStudentBySearchClassID(request.getParameter("search"));
                }
            }
            
            request.setAttribute("TeacherList", Student);
            targetURL = "studentModifity.jsp";
        }else if("changeStudentfin".equalsIgnoreCase(action)) {
            if(changeStudent(request,response)==true){
            request.setAttribute("check","Change Successful" );
            }else{
            request.setAttribute("check","Change is not Successful, Please try again" );
            }
            ArrayList<Student> TeacherList = getStudent(request,response);
            request.setAttribute("TeacherList", TeacherList);
            targetURL = "studentModifity.jsp";
        }else if("modifityTeacher".equalsIgnoreCase(action)) {
            ArrayList TeacherList = getTeacher(request,response);
            if(request.getParameter("search")!= null){
                if(request.getParameter("type").equals( "1")){     
                TeacherList = db.getTeatchBySearchID(request.getParameter("search"));
                }else if(request.getParameter("type").equals("2")){
                TeacherList = db.getTeatchBySearchName(request.getParameter("search"));
                }else if(request.getParameter("type").equals("3")){
                TeacherList = db.getTeatchBySearchRole(request.getParameter("search"));
                }
            }
            request.setAttribute("TeacherList", TeacherList);
            targetURL = "teacherModifity.jsp";
        }else if("modifityTeacherfin".equalsIgnoreCase(action)) {
            if(changeTeacher(request,response)==true){
            request.setAttribute("check","Change Teacher Successful" );
            }else{
            request.setAttribute("check","Change Teacher not Successful, Please try again" );
            }
            ArrayList TeacherList = getTeacher(request,response);
            request.setAttribute("TeacherList", TeacherList);
            targetURL = "teacherModifity.jsp";
        }else if("modifityProfile".equalsIgnoreCase(action)) {
            ArrayList<Admin> profile = getProfile(request,response);
             request.setAttribute("profile",profile);
            targetURL = "profileModifity.jsp";
        }if("modifityProfilefin".equalsIgnoreCase(action)) {
            
             if(changeProfile(request,response)==true){
            request.setAttribute("check","Change Profile Successful" );
            }else{
            request.setAttribute("check","Change Profile not Successful, Please try again" );
            }
            ArrayList<Admin> profile = getProfile(request,response);
             request.setAttribute("profile",profile);
            targetURL = "profileModifity.jsp";
        }
        
        
        RequestDispatcher rd;
        rd = getServletContext().getRequestDispatcher("/" + targetURL);
        rd.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    protected ArrayList<Program> getProgram(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        return db.getProgram();
    }
        protected ArrayList getProgramClass(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
           String ID = request.getParameter("program");
        return db.getProgramClass(ID);
    }
    protected boolean addUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
           String StudentID = request.getParameter("studentID");
           String ClassID = request.getParameter("class");
           String ProgramID = request.getParameter("program");
           String StudentName = request.getParameter("studentName");
           String Password = request.getParameter("password");
        return db.addStudent(StudentID,ClassID,ProgramID,StudentName,Password);
    }
        protected boolean addTeacher(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
           String teacherID = request.getParameter("teacherID");
           String permisson = request.getParameter("permisson");
           String teacherName = request.getParameter("teacherName");
           String Password = request.getParameter("password");
        return db.addTeacher(teacherID,permisson,teacherName,Password);
    }
        protected ArrayList<Teacher> getTeacher(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        return db.getTeatch();
    }
     protected ArrayList<Teacher> getTeacherbySearch(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        return db.getTeatch();
    }
    protected ArrayList<Student> getStudent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        return db.getStudent();
    }
       protected boolean changeRole(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
              String teacherID = request.getParameter("id");
           String role = request.getParameter("role");
        return db.changeTeacherRole(teacherID, role);
    }
        protected boolean changeStudent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
              String teacherID = request.getParameter("id");
                 String name = request.getParameter("name");
                 String password = request.getParameter("password");
        return db.changeStudent(teacherID, name,password);
    }
    protected boolean changeTeacher(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
              String teacherID = request.getParameter("id");
                 String name = request.getParameter("name");
                 String password = request.getParameter("password");
                 String role = request.getParameter("role");
        return db.changeTeacher(teacherID, name,password,role);
    }
        protected  ArrayList<Admin> getProfile(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      HttpSession session = request.getSession();
      AdminInfo adminInfo = (AdminInfo)session.getAttribute("userInfo");

      System.out.print(adminInfo.getStringId());
        return db.getAdmin(adminInfo.getStringId());
    }
        protected boolean changeProfile(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
              String ID = request.getParameter("id");
                 String name = request.getParameter("name");
                 String password = request.getParameter("password");
        return db.changeProfile(ID, name,password);
    }
}
