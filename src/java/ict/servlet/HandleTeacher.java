/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ict.servlet;

import ict.bean.*;
import ict.db.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author tommylam
 */
@WebServlet(name = "HandleTeacher", urlPatterns = {"/teacher"})
public class HandleTeacher extends HttpServlet {

    private ClassDB db;

    public void init() {
        String dbUser = this.getServletContext().getInitParameter("dbUser");
        String dbPassword = this.getServletContext().getInitParameter("dbPassword");
        String dbUrl = this.getServletContext().getInitParameter("dbUrl");
        db = new ClassDB(dbUrl, dbUser, dbPassword);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        String targetURL = "";
        
        if ("module".equalsIgnoreCase(action)) {
            request.setAttribute("Module", module(request, response));
            targetURL = "teacherModule.jsp";
        } else if ("class".equalsIgnoreCase(action)) {
            request.setAttribute("classInfo", classInfo(request, response));
            targetURL = "teacherClass.jsp";
        }else if ("ReportClassList".equalsIgnoreCase(action)) {
            request.setAttribute("classInfo", classInfo(request, response));
            targetURL = "reportClassList.jsp";
        } else if ("report".equalsIgnoreCase(action)) {
             request.setAttribute("Module", module(request, response));
            targetURL = "teacherReport.jsp";
        } else if ("showReport".equalsIgnoreCase(action)) {
            request.setAttribute("Student", getStudent(request, response));
            request.setAttribute("lesson", lesson(request, response));
            targetURL = "showReport.jsp";
        } else if ("showFailReport".equalsIgnoreCase(action)) {
            request.setAttribute("Student", getStudent(request, response));
            request.setAttribute("lesson", lesson(request, response));
            targetURL = "showFailReport.jsp";
        } else if ("lesson".equalsIgnoreCase(action)) {
            request.setAttribute("lesson", lesson(request, response));
            targetURL = "teacherLesson.jsp";
        } else if ("addLesson".equalsIgnoreCase(action)) {
            request.setAttribute("message", createLesson(request, response));
            request.setAttribute("lesson", lesson(request, response));
            targetURL = "teacherLesson.jsp";
        } else if ("attendent".equalsIgnoreCase(action)) {
            request.setAttribute("attendentList", attendent(request, response));
            targetURL = "teacherAttendent.jsp";
        } else if ("updateAttend".equalsIgnoreCase(action)) {
            request.setAttribute("message", updateAttend(request, response));
            request.setAttribute("attendentList", attendent(request, response));
            targetURL = "teacherAttendent.jsp";
        } else if ("createAttend".equalsIgnoreCase(action)) {
            request.setAttribute("message", createAttend(request, response));
            request.setAttribute("attendentList", attendent(request, response));
            targetURL = "teacherAttendent.jsp";
        } else if ("deleteLesson".equalsIgnoreCase(action)) {
            request.setAttribute("message", deleteLesson(request, response));
            request.setAttribute("lesson", lesson(request, response));
            targetURL = "teacherLesson.jsp";
        } else {
            PrintWriter out = response.getWriter();
            out.println("No such action!!!");
        }
        RequestDispatcher rd;
        rd = getServletContext().getRequestDispatcher("/" + targetURL);
        rd.forward(request, response);
    }

    protected ArrayList<LessonInfo> module(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String search = request.getParameter("search");
        ArrayList<LessonInfo> module = new ArrayList();
        HttpSession session = request.getSession();
        TeacherInfo teacherInfo = (TeacherInfo) session.getAttribute("userInfo");
        if ("search".equalsIgnoreCase(search)) {
            String value = request.getParameter("value");
            module = db.getModuleByTeacherSearch(Integer.toString(teacherInfo.getId()), value);
        } else {
            module = db.getModuleByTeacher(Integer.toString(teacherInfo.getId()));
        }
        return module;
    }

    protected ArrayList<ClassInfo> classInfo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String search = request.getParameter("search");
        ArrayList<ClassInfo> classInfo = new ArrayList();
        HttpSession session = request.getSession();
        String ModuleID = request.getParameter("moduleID");
        TeacherInfo teacherInfo = (TeacherInfo) session.getAttribute("userInfo");
        if ("search".equalsIgnoreCase(search)) {
            String value = request.getParameter("value");
            classInfo = db.getClassBySearch(Integer.toString(teacherInfo.getId()), ModuleID, value);
        } else {
            classInfo = db.getClass(Integer.toString(teacherInfo.getId()), ModuleID);
        }
        return classInfo;
    }

    protected ArrayList<Attendent> lesson(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String search = request.getParameter("search");
        ArrayList<Attendent> lesson = new ArrayList();
        HttpSession session = request.getSession();
        String ModuleID = request.getParameter("moduleID");
        String ProgramID = request.getParameter("programID");
        String ClassID = request.getParameter("classID");
        TeacherInfo teacherInfo = (TeacherInfo) session.getAttribute("userInfo");
        if ("date".equalsIgnoreCase(search)) {
            String fDate = request.getParameter("fDate");
            String tDate = request.getParameter("tDate");
            lesson = db.getLessonByModuleDate(ProgramID, ClassID, ModuleID, fDate, tDate);
        } else {
            lesson = db.getLessonByModule(ProgramID, ClassID, ModuleID);
        }
        return lesson;
    }

    protected ArrayList<AttendentList> attendent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ArrayList<AttendentList> attendentList = new ArrayList();
        HttpSession session = request.getSession();
        int lessonID = Integer.parseInt(request.getParameter("LessonID"));
        attendentList = db.getAttendentList(lessonID);
        return attendentList;
    }
    
      protected ArrayList<Report> getStudent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ArrayList<Report> attendentList = new ArrayList();
        HttpSession session = request.getSession();
        String ModuleID = request.getParameter("moduleID");
        String ProgramID = request.getParameter("programID");
        String ClassID = request.getParameter("classID");
        attendentList = db.StudentList(ModuleID,ClassID,ProgramID);
        return attendentList;
    }
    protected String updateAttend(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ArrayList<AttendentList> attendentList = new ArrayList();
        HttpSession session = request.getSession();
        int lessonID = Integer.parseInt(request.getParameter("LessonID"));
        int studentID = Integer.parseInt(request.getParameter("studentID"));
        if (request.getParameter("attendTime") == null || request.getParameter("attendTime") == "") {
            if (db.deleteAttendent(lessonID, studentID)) {
                return "Update Success!";
            } else {
                return "Update Unsuccess!";
            }
        } else {
            String attendTime = request.getParameter("attendTime");
            if (db.updateAttendent(lessonID, studentID, attendTime)) {
                return "Update Success!";
            } else {
                return "Update Unsuccess!";
            }
        }
    }

    protected String createAttend(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ArrayList<AttendentList> attendentList = new ArrayList();
        HttpSession session = request.getSession();
        int lessonID = Integer.parseInt(request.getParameter("LessonID"));
        int studentID = Integer.parseInt(request.getParameter("studentID"));
        String attendTime = request.getParameter("attendTime");
        if (db.createAttendent(lessonID, studentID, attendTime)) {
            return "Update Success!";
        } else {
            return "Update Unsuccess!";
        }
    }

    protected String createLesson(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ArrayList<AttendentList> attendentList = new ArrayList();
        HttpSession session = request.getSession();
        String ModuleID = request.getParameter("moduleID");
        String ClassID = request.getParameter("classID");
        String ProgramID = request.getParameter("programID");
        String Date = request.getParameter("lessonDate");
        String Time = request.getParameter("lessonTime");
        Double Duration = Double.parseDouble(request.getParameter("lessonDuration"));
        if (db.createLesson(ModuleID, ClassID, ProgramID, Date, Time, Duration)) {
            return "Create Success!";
        } else {
            return "Create Unsuccess!";
        }
    }

    protected String deleteLesson(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ArrayList<AttendentList> attendentList = new ArrayList();
        HttpSession session = request.getSession();
        int lessonID = Integer.parseInt(request.getParameter("LessonID"));
        if (db.deleteLesson(lessonID)) {
            return "Delete Success!";
        } else {
            return "Delete Unsuccess!";
        }
    }
}
