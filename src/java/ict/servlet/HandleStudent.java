/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ict.servlet;

import ict.bean.*;
import ict.db.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author tommylam
 */
@WebServlet(name = "HandleStudent", urlPatterns = {"/student"})
public class HandleStudent extends HttpServlet {

    private ClassDB db;

    public void init() {
        String dbUser = this.getServletContext().getInitParameter("dbUser");
        String dbPassword = this.getServletContext().getInitParameter("dbPassword");
        String dbUrl = this.getServletContext().getInitParameter("dbUrl");
        db = new ClassDB(dbUrl, dbUser, dbPassword);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        String targetURL = "";
        if ("schedule".equalsIgnoreCase(action)) {
            ArrayList<LessonInfo> lesson = schedule(request, response);
            request.setAttribute("LessonInfo", lesson);
            targetURL = "schedule.jsp";
        } else if ("module".equalsIgnoreCase(action)) {
            ArrayList<LessonInfo> module = module(request, response);
            request.setAttribute("Module", module);
            targetURL = "studModule.jsp";
        } else if ("attendent".equalsIgnoreCase(action)) {
            ArrayList<Attendent> attendent = attendent(request, response);
            request.setAttribute("Attendent", attendent);
            targetURL = "studAttendent.jsp";
        } else {
            PrintWriter out = response.getWriter();
            out.println("No such action!!!");
        }
        RequestDispatcher rd;
        rd = getServletContext().getRequestDispatcher("/" + targetURL);
        rd.forward(request, response);
    }

    protected ArrayList<LessonInfo> schedule(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String search = request.getParameter("search");
        ArrayList<LessonInfo> lesson = new ArrayList();
        if ("date".equalsIgnoreCase(search)) {
            HttpSession session = request.getSession();
            StudentInfo studentInfo = (StudentInfo) session.getAttribute("userInfo");
            String fDate = request.getParameter("fDate");
            String tDate = request.getParameter("tDate");
            lesson = db.getLessonByDate(studentInfo.getProgramID(), studentInfo.getClassID(), fDate, tDate);
        } else {
            HttpSession session = request.getSession();
            StudentInfo studentInfo = (StudentInfo) session.getAttribute("userInfo");
            lesson = db.getLesson(studentInfo.getProgramID(), studentInfo.getClassID());
        }
        return lesson;
    }

    protected ArrayList<LessonInfo> module(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String search = request.getParameter("search");
        ArrayList<LessonInfo> module = new ArrayList();
        HttpSession session = request.getSession();
        StudentInfo studentInfo = (StudentInfo) session.getAttribute("userInfo");
        if ("search".equalsIgnoreCase(search)) {
            String value = request.getParameter("value");
            module = db.getModuleBySearch(studentInfo.getClassID(), studentInfo.getProgramID(), value);
        } else {
            module = db.getModule(studentInfo.getClassID(), studentInfo.getProgramID());
        }
        return module;
    }

    protected ArrayList<Attendent> attendent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        StudentInfo studentInfo = (StudentInfo) session.getAttribute("userInfo");
        String ClassID = studentInfo.getClassID();
        String ProgramID = studentInfo.getProgramID();
        String ModuleID = request.getParameter("moduleID");
        String StudentID = Integer.toString(studentInfo.getId());
        return db.getAttendent(ClassID, ProgramID, ModuleID, StudentID) ;
    }
}
