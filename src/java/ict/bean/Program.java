/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ict.bean;

import java.util.ArrayList;

/**
 *
 * @author 85253
 */
public class Program {
    ArrayList classList;
    String ProgramID;

    public Program(ArrayList classList, String ProgramID) {
        this.classList = classList;
        this.ProgramID = ProgramID;
    }

    public ArrayList getClassList() {
        return classList;
    }

    public void setClassList(ArrayList classList) {
        this.classList = classList;
    }

    public String getProgramID() {
        return ProgramID;
    }

    public void setProgramID(String ProgramID) {
        this.ProgramID = ProgramID;
    }
    
    
}
