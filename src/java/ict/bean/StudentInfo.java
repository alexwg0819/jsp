/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ict.bean;

/**
 *
 * @author tommylam
 */
public class StudentInfo extends UserInfo {

    private String programID;
    private String classID;

    public StudentInfo() {
    }

    public StudentInfo(String programID, String classID) {
        this.programID = programID;
        this.classID = classID;
    }

    public StudentInfo(String programID, String classID, int id, String name, String role) {
        super(id, name, role);
        this.programID = programID;
        this.classID = classID;
    }

    public String getProgramID() {
        return programID;
    }

    public String getClassID() {
        return classID;
    }

    public void setProgramID(String programID) {
        this.programID = programID;
    }

    public void setClassID(String classID) {
        this.classID = classID;
    }
}
