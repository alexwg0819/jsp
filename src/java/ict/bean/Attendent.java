/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ict.bean;

import java.util.Date;

/**
 *
 * @author tommylam
 */
public class Attendent extends LessonInfo {

    private Date AttendTime;
    private int LessonID;

    public Attendent() {
    }

    public Attendent(Date AttendTime, int LessonID) {
        this.AttendTime = AttendTime;
        this.LessonID = LessonID;
    }

    public Attendent(Date AttendTime, int LessonID, String ModuleName, String ModuleID, String TeacherName, Date LessonDate, Date LessonTime, double LessonDuration) {
        super(ModuleName, ModuleID, TeacherName, LessonDate, LessonTime, LessonDuration);
        this.AttendTime = AttendTime;
        this.LessonID = LessonID;
    }

    public Date getAttendTime() {
        return AttendTime;
    }

    public int getLessonID() {
        return LessonID;
    }

    public String getModuleName() {
        return ModuleName;
    }

    public String getModuleID() {
        return ModuleID;
    }

    public String getTeacherName() {
        return TeacherName;
    }

    public Date getLessonDate() {
        return LessonDate;
    }

    public Date getLessonTime() {
        return LessonTime;
    }

    public double getLessonDuration() {
        return LessonDuration;
    }

    public void setAttendTime(Date AttendTime) {
        this.AttendTime = AttendTime;
    }

    public void setLessonID(int LessonID) {
        this.LessonID = LessonID;
    }

    public void setModuleName(String ModuleName) {
        this.ModuleName = ModuleName;
    }

    public void setModuleID(String ModuleID) {
        this.ModuleID = ModuleID;
    }

    public void setTeacherName(String TeacherName) {
        this.TeacherName = TeacherName;
    }

    public void setLessonDate(Date LessonDate) {
        this.LessonDate = LessonDate;
    }

    public void setLessonTime(Date LessonTime) {
        this.LessonTime = LessonTime;
    }

    public void setLessonDuration(double LessonDuration) {
        this.LessonDuration = LessonDuration;
    }
}
