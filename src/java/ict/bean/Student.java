/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ict.bean;

/**
 *
 * @author 85253
 */
public class Student {

    public String getStudentID() {
        return StudentID;
    }

    public Student(String StudentID, String ClassID, String ProgramID, String StudentName, String Password) {
        this.StudentID = StudentID;
        this.ClassID = ClassID;
        this.ProgramID = ProgramID;
        this.StudentName = StudentName;
        this.Password = Password;
    }

    public void setStudentID(String StudentID) {
        this.StudentID = StudentID;
    }

    public String getClassID() {
        return ClassID;
    }

    public void setClassID(String ClassID) {
        this.ClassID = ClassID;
    }

    public String getProgramID() {
        return ProgramID;
    }

    public void setProgramID(String ProgramID) {
        this.ProgramID = ProgramID;
    }

    public String getStudentName() {
        return StudentName;
    }

    public void setStudentName(String StudentName) {
        this.StudentName = StudentName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }
    String StudentID;
    String ClassID;
    String ProgramID;
    String StudentName;
    String Password;
}
