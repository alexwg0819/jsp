/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ict.bean;

/**
 *
 * @author tommylam
 */
public class TeacherInfo extends UserInfo {

    private int permission;

    public TeacherInfo() {
    }

    public TeacherInfo(int permission) {
        this.permission = permission;
    }

    public TeacherInfo(int permission, int id, String name, String role) {
        super(id, name, role);
        this.permission = permission;
    }

    public int getPermission() {
        return permission;
    }

    public void setPermission(int permission) {
        this.permission = permission;
    }
}
