/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ict.bean;

import java.util.Date;

/**
 *
 * @author tommylam
 */
public class AttendentList {

    String ProgramName, ProgramID, ClassID, ModuleName, ModuleID, StudentName;
    Date lessonDate, lessonTime, attendDate;
    int StudentID;
    double duration;

    public AttendentList() {
    }

    public AttendentList(String ProgramName, String ProgramID, String ClassID, String ModuleName, String ModuleID, String StudentName, Date lessonDate, Date lessonTime, Date attendDate, int StudentID, double duration) {
        this.ProgramName = ProgramName;
        this.ProgramID = ProgramID;
        this.ClassID = ClassID;
        this.ModuleName = ModuleName;
        this.ModuleID = ModuleID;
        this.StudentName = StudentName;
        this.lessonDate = lessonDate;
        this.lessonTime = lessonTime;
        this.attendDate = attendDate;
        this.StudentID = StudentID;
        this.duration = duration;
    }

    public String getProgramName() {
        return ProgramName;
    }

    public void setProgramName(String ProgramName) {
        this.ProgramName = ProgramName;
    }

    public String getProgramID() {
        return ProgramID;
    }

    public void setProgramID(String ProgramID) {
        this.ProgramID = ProgramID;
    }

    public String getClassID() {
        return ClassID;
    }

    public void setClassID(String ClassID) {
        this.ClassID = ClassID;
    }

    public String getModuleName() {
        return ModuleName;
    }

    public void setModuleName(String ModuleName) {
        this.ModuleName = ModuleName;
    }

    public String getModuleID() {
        return ModuleID;
    }

    public void setModuleID(String ModuleID) {
        this.ModuleID = ModuleID;
    }

    public String getStudentName() {
        return StudentName;
    }

    public void setStudentName(String StudentName) {
        this.StudentName = StudentName;
    }

    public Date getLessonDate() {
        return lessonDate;
    }

    public void setLessonDate(Date lessonDate) {
        this.lessonDate = lessonDate;
    }

    public Date getLessonTime() {
        return lessonTime;
    }

    public void setLessonTime(Date lessonTime) {
        this.lessonTime = lessonTime;
    }

    public Date getAttendDate() {
        return attendDate;
    }

    public void setAttendDate(Date attendDate) {
        this.attendDate = attendDate;
    }

    public int getStudentID() {
        return StudentID;
    }

    public void setStudentID(int StudentID) {
        this.StudentID = StudentID;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }
}