/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ict.bean;

import java.util.*;

/**
 *
 * @author tommylam
 */
public class LessonInfo {

    String ModuleName, ModuleID, TeacherName;
    Date LessonDate, LessonTime;
    double LessonDuration;

    public LessonInfo() {
    }

    public LessonInfo(String ModuleName, String ModuleID, String TeacherName, Date LessonDate, Date LessonTime, double LessonDuration) {
        this.ModuleName = ModuleName;
        this.ModuleID = ModuleID;
        this.TeacherName = TeacherName;
        this.LessonDate = LessonDate;
        this.LessonTime = LessonTime;
        this.LessonDuration = LessonDuration;
    }

    public String getModuleName() {
        return ModuleName;
    }

    public String getModuleID() {
        return ModuleID;
    }

    public String getTeacherName() {
        return TeacherName;
    }

    public Date getLessonDate() {
        return LessonDate;
    }

    public Date getLessonTime() {
        return LessonTime;
    }

    public double getLessonDuration() {
        return LessonDuration;
    }

    public void setModuleName(String ModuleName) {
        this.ModuleName = ModuleName;
    }

    public void setModuleID(String ModuleID) {
        this.ModuleID = ModuleID;
    }

    public void setTeacherName(String TeacherName) {
        this.TeacherName = TeacherName;
    }

    public void setLessonDate(Date LessonDate) {
        this.LessonDate = LessonDate;
    }

    public void setLessonTime(Date LessonTime) {
        this.LessonTime = LessonTime;
    }

    public void setLessonDuration(double LessonDuration) {
        this.LessonDuration = LessonDuration;
    }
}
