package ict.bean;

import ict.db.ClassDB;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 85253
 */
public class Report {
    double duration = 0;
    double attendent = 0;
    String ClassID;
    String ProgramID;
    String ModuleID;
//    private ClassDB db;
    String StudentName;
    String ID;
    ArrayList<Attendent> attendentl;

//    public void init() {
//        String dbUser = "root";
//        String dbPassword = "";
//        String dbUrl = "jdbc:mysql://localhost:3306/Attendance_System";
//        db = new ClassDB(dbUrl, dbUser, dbPassword);
//    }

    public double getAttendent() {
        return attendent;
    }

    public void setAttendent(double attendent) {
        this.attendent = attendent;
    }
    public String getStudentName() {
        return StudentName;
    }

    public void setStudentName(String StudentName) {
        this.StudentName = StudentName;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    public String getClassID() {
        return ClassID;
    }

    public void setClassID(String ClassID) {
        this.ClassID = ClassID;
    }

    public String getProgramID() {
        return ProgramID;
    }

    public void setProgramID(String ProgramID) {
        this.ProgramID = ProgramID;
    }

    public String getModuleID() {
        return ModuleID;
    }

    public void setModuleID(String ModuleID) {
        this.ModuleID = ModuleID;
    }

    public ArrayList<Attendent> getAttendentl() {
        return attendentl;
    }

    public void setAttendentl(ArrayList<Attendent> attendentl) {
        this.attendentl = attendentl;
    }

    public Report(String StudentName,String ID,String ClassID,String ProgramID, String ModuleID,ArrayList<Attendent> attendent){
        this.StudentName = StudentName;
        this.ID = ID;
        this.ClassID = ClassID;
        this.ProgramID = ProgramID;
        this.ModuleID = ModuleID;
        this.attendentl =attendent;
    }
    
    public void addDuration(double hour){
        duration += hour;
    }
    public double showAttd(double total){
                                    double att = 0;
                            for(int b = 0;b<attendentl.size();b++){
                                    
                                    if(attendentl.get(b).getAttendTime() != null){
                                        if(attendentl.get(b).getAttendTime().before(attendentl.get(b).getLessonTime()) || attendentl.get(b).getLessonTime().equals(attendentl.get(b).getAttendTime())){
                                        att += attendentl.get(b).getLessonDuration();
                                        }else{
                                        att += (attendentl.get(b).getAttendTime().getTime()-attendentl.get(b).getLessonTime().getTime())/3600000;                   
                                        }
                                    }  
                             }
        return convert(att/total*100);
    }
          static   double   convert(double   value){  
          long   l1   =   Math.round(value*100); 
          double   ret   =   l1/100.0;               
          return   ret;  
      }  
//    public long getDuration(){

       
//       for (int i = 0; i < attendentl.size(); i++) {
//                  if(attendentl.get(i).getAttendTime() != null){
//                    if (attendentl.get(i).getAttendTime().before(attendentl.get(i).getLessonTime()) || attendentl.get(i).getLessonTime().equals(attendentl.get(i).getAttendTime())) {
//                        return  attendentl.get(i).getLessonDuration();
////                        totaltime += attendentl.get(i).getLessonDuration();
//                    } else {
////                               Date date1 = attendentl.get(0).getAttendTime();
////                               Date date2 = attendentl.get(0).getLessonTime();
////                               long cat = date1.getTime()- date2.getTime();
////                               long diffHours = cat / (60 * 60 * 1000) % 24;
////                               totaltime += diffHours;
//                    }
//                }else{
//                    
//                }
//       }

//    }
}
