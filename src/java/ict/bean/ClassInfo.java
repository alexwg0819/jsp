/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ict.bean;

/**
 *
 * @author tommylam
 */
public class ClassInfo {

    String ClassID, ProgramName, ProgramID, ModuleID;

    public ClassInfo() {
    }

    public ClassInfo(String ClassID, String ProgramName, String ProgramID, String ModuleID) {
        this.ClassID = ClassID;
        this.ProgramName = ProgramName;
        this.ProgramID = ProgramID;
        this.ModuleID = ModuleID;
    }

    public String getClassID() {
        return ClassID;
    }

    public String getProgramName() {
        return ProgramName;
    }

    public String getProgramID() {
        return ProgramID;
    }

    public String getModuleID() {
        return ModuleID;
    }

    public void setClassID(String ClassID) {
        this.ClassID = ClassID;
    }

    public void setProgramName(String ProgramName) {
        this.ProgramName = ProgramName;
    }

    public void setProgramID(String ProgramID) {
        this.ProgramID = ProgramID;
    }

    public void setModuleID(String ModuleID) {
        this.ModuleID = ModuleID;
    }
}
