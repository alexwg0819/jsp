/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ict.bean;

/**
 *
 * @author 85253
 */
public class Teacher {
    String name;
    String Password;

    public String getName() {
        return name;
    }

    public Teacher(String name, String id, String role) {
        this.name = name;
        this.id = id;
        this.role = role;
    }
    public Teacher(String name, String id, String role, String  Password) {
        this.name = name;
        this.id = id;
        this.role = role;
        this.Password = Password;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
    String id;
    String role;
}
